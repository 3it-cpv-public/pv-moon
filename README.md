![3IT Logo](ressources/3IT_VH-C.png "3IT")

# cpv-moon

Software project to evaluate the theoretical electrical relevance of a PV module installed on the Moon.

The algorithm mainly allows to model a PV surface in a location on the Moon's ground and predict the
theoretical solar energy received for the period and the setting of the user's choice.

To get this production, the program is able to compute the azimuth and the elevation angles of the Sun in the lunar sky
from a chosen position on the Moon's surface and a chosen moment in UTC.

## To begin

The following instructions will allow you to:

* Install the software's prerequisites to this project
* Get a copy of this project in your development environment
* Build the application locally
* Start the main applications of the programm locally

### Prerequisites

* Python 3.9+

### Beforehand

Get the source code in the repository of your choice:

```
git clone https://gitlab-acad.gegi.usherbrooke.ca/3it/cpv-moon
```

### Installation

It is necessary to install the project's required dependencies:

```
pip3 install -r requirements.txt
```

## Usage

### Modeling the Sun position in the lunar sky

The first objective of the project is to compute the azimuth and elevation angles of the Sun in the lunar sky for chosen
point and moment using the class PositionSunLunarSky in the module ***position_sun_lunar_sky.py***.

### Modeling PV or CPV module and the theoretical electrical solar energy received on the Moon

The project allows also to model a PV or CPV surface and the theoretical electrical solar energy received on the
lunar ground. The package src\app gives two examples of what the program is able to do.

#### Getting the maximum irradiation received for the moon year 2023, for a point at the equator and with a

#### fixed surface

To get this example, use the following command:

```
cd src
python3 -m app.getting_irradiation
```

First, the example generates the Sun's elevations and azimuths for the period and the coordinates on the equator using
***export_sun_elevations*** and ***export_sun_azimuths***.
In parallel, it also generates the lunar global irradiance for the period using
***export_global_lunar_irradiance***.
The next step is to use the three files generated before to generate the irradiance when the Sun is visible during the
period and at the location using ***export_irradiance_when_sun_visible***. Finally, the program exports the maximum
irradiation for a fixed surface and a dual tracker using ***export_maximum_irradiation***.

#### Plotting the elevations of the Sun at a point on the equator for the lunar year 2023

To get this example, use the following command:

```
cd src
python3 -m app.plotting_elevations_sun
```

First, the example exports the Sun's elevations for the period using ***export_sun_elevations***. Then it reads the file
generated to create a dataframe with the elevations using the pandas function ***read_csv***. And finally it plots the
elevations as a function of the hours in the period.

## Execute the tests

To execute the tests (unit), you have to execute the following commands:

```
 cd src
 python3 -m unittest discover -s ../tests/unit/
```

### Verify the coverage of the tests

To verify tests coverage:

```
 cd src
 coverage run --source=. -m  unittest discover -s ../tests/unit/
 coverage report
```
