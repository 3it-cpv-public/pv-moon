from datetime import datetime
from unittest import TestCase

import numpy as np

from cpv_moon.subsolar_point_coord import SubsolarPointCoordinate


class TestSubsolarPointCoordinate(TestCase):

    def test_get_optical_seleno_longitude_sun(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = SubsolarPointCoordinate(date, date, frequency)

        # when
        optical_seleno_longitude_sun = test_system.get_optical_seleno_longitude_sun()
        optical_seleno_longitude_sun_360 = optical_seleno_longitude_sun % 360

        # then
        np.testing.assert_array_almost_equal(67.920, optical_seleno_longitude_sun_360, 3)

    def test_get_optical_seleno_latitude_sun(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = SubsolarPointCoordinate(date, date, frequency)

        # when
        optical_seleno_latitude_sun = test_system.get_optical_seleno_latitude_sun()
        optical_seleno_latitude_sun_90 = ((optical_seleno_latitude_sun - 90) % 180) - 90

        # then
        np.testing.assert_array_almost_equal(1.476, optical_seleno_latitude_sun_90, 3)

    def test_get_physical_seleno_longitude_sun(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = SubsolarPointCoordinate(date, date, frequency)

        # when
        physical_seleno_longitude_sun = test_system.get_physical_seleno_longitude_sun()
        physical_seleno_longitude_sun_180 = ((physical_seleno_longitude_sun - 180) % 360) - 180

        # then
        np.testing.assert_array_almost_equal(-0.026, physical_seleno_longitude_sun_180, 3)

    def test_get_physical_seleno_latitude_sun(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = SubsolarPointCoordinate(date, date, frequency)

        # when
        physical_seleno_latitude_sun = test_system.get_physical_seleno_latitude_sun()
        physical_seleno_latitude_sun_90 = ((physical_seleno_latitude_sun - 90) % 180) - 90

        # then
        np.testing.assert_array_almost_equal(-0.015, physical_seleno_latitude_sun_90, 3)

    def test_get_selenographic_longitude_subsolar_point(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = SubsolarPointCoordinate(date, date, frequency)

        # when
        selenographic_longitude_subsolar_point = test_system.get_selenographic_longitude_subsolar_point()

        # then
        np.testing.assert_array_almost_equal(67.89, selenographic_longitude_subsolar_point, 2)

    def test_get_selenographic_latitude_subsolar_point(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = SubsolarPointCoordinate(date, date, frequency)

        # when
        selenographic_latitude_subsolar_point = test_system.get_selenographic_latitude_subsolar_point()

        # then
        np.testing.assert_array_almost_equal(1.46, selenographic_latitude_subsolar_point, 2)
