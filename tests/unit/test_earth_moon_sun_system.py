from datetime import datetime
from unittest import TestCase

import numpy as np

from cpv_moon.earth_moon_sun_system import EarthMoonSunSystem


class TestEarthMoonSunSystem(TestCase):

    def test_get_sun_earth_distance(self):
        # given
        date_start = datetime(year=1992, month=10, day=13, hour=0, minute=0, second=0)
        date_end = datetime(year=1992, month=10, day=14, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = EarthMoonSunSystem(date_start, date_end, frequency)

        # when
        sun_earth_distance = test_system.get_sun_earth_distance()

        # then
        results = [0.99766195, 0.99765006, 0.99763818, 0.99762629, 0.99761441, 0.99760253,
                   0.99759064, 0.99757876, 0.99756688, 0.997555,   0.99754313, 0.99753125,
                   0.99751938, 0.9975075,  0.99749563, 0.99748376, 0.99747189, 0.99746002,
                   0.99744815, 0.99743628, 0.99742441, 0.99741255, 0.99740069, 0.99738882,
                   0.99737696]
        np.testing.assert_array_almost_equal(results, sun_earth_distance, 5)

    def test_get_earth_moon_distance(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = EarthMoonSunSystem(date, date, frequency)

        # when
        earth_moon_distance = test_system.get_earth_moon_distance()

        # then
        np.testing.assert_array_almost_equal(368409.7, earth_moon_distance, 1)

    def test_get_geocentric_longitude_center_moon(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = EarthMoonSunSystem(date, date, frequency)

        # when
        geocentric_longitude_center_moon = test_system.get_geocentric_longitude_center_moon()

        # then
        np.testing.assert_array_almost_equal(133.162659, geocentric_longitude_center_moon, 6)

    def test_get_geocentric_latitude_center_moon(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = EarthMoonSunSystem(date, date, frequency)

        # when
        geocentric_latitude_center_moon = test_system.get_geocentric_latitude_center_moon()

        # then
        np.testing.assert_array_almost_equal(-3.229127, geocentric_latitude_center_moon, 4)

    def test_get_nutation_earth(self):
        # given
        date = datetime(year=1987, month=4, day=10, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = EarthMoonSunSystem(date, date, frequency)

        # when
        nutation_earth = test_system.get_nutation_earth()

        # then
        np.testing.assert_array_almost_equal(-0.001052, nutation_earth, 5)

    def test_get_heliocentric_ecliptical_longitude_moon(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = EarthMoonSunSystem(date, date, frequency)

        # when
        heliocentric_ecliptical_longitude_moon = test_system.get_heliocentric_ecliptical_longitude_moon()

        # then
        np.testing.assert_array_almost_equal(202.208438, heliocentric_ecliptical_longitude_moon, 3)

    def test_get_heliocentric_ecliptical_latitude_moon(self):
        # given
        date = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)
        frequency = 'H'
        test_system = EarthMoonSunSystem(date, date, frequency)

        # when
        heliocentric_ecliptical_latitude_moon = test_system.get_heliocentric_ecliptical_lat_moon()

        # then
        np.testing.assert_array_almost_equal(-0.007932, heliocentric_ecliptical_latitude_moon, 6)
