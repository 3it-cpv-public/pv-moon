from datetime import datetime
from unittest import TestCase

import numpy as np

from cpv_moon.coordinate import Coordinate
from cpv_moon.position_sun_lunar_sky import PositionSunLunarSky
from cpv_moon.subsolar_point_coord import SubsolarPointCoordinate


class TestPositionSunLunarSky(TestCase):
    """
    Testing the functions of the position_sun_lunar_sky module
    """
    def test_get_azimuth_sun(self):
        """
        The truth is based on a first run of the function. But coordinate and moment have been choose to make the
        longitude and the latitude of the point correspond to the subsolar point.
        Elevation angle is closed to 90° so approximation of a real truth for the elevation angle seems good.

        """
        # given
        latitude = -0.045
        longitude = 114.557
        altitude = 0
        coordinates = Coordinate(latitude, longitude, altitude)
        date = datetime(year=2020, month=1, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        lunar_sky = PositionSunLunarSky(date, date, frequency)

        # when
        azimuth_sun = lunar_sky.get_sun_azimuth(coordinates)

        # then
        np.testing.assert_array_almost_equal(120.232230, azimuth_sun, 5)

    def test_get_elevation_sun(self):
        # given
        latitude = -0.045
        longitude = 114.557
        altitude = 0
        coordinates = Coordinate(latitude, longitude, altitude)
        date = datetime(year=2020, month=1, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        lunar_sky = PositionSunLunarSky(date, date, frequency)

        # when
        elevation_sun = lunar_sky.get_sun_elevation(coordinates)

        # then
        np.testing.assert_array_almost_equal(89.987379, elevation_sun, 5)

    def test_get_sun_azimuth_from_arrays(self):
        # given
        latitude = -0.045
        longitude = 114.557
        altitude = 0
        coordinates = Coordinate(latitude, longitude, altitude)
        date = datetime(year=2020, month=1, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        subsolar_point = SubsolarPointCoordinate(date, date, frequency)
        longitudes = subsolar_point.get_selenographic_longitude_subsolar_point()
        latitudes = subsolar_point.get_selenographic_latitude_subsolar_point()
        lunar_sky = PositionSunLunarSky(date, date, frequency)

        # when
        azimuth_sun = lunar_sky.get_sun_azimuth_from_arrays(coordinates, longitudes, latitudes)

        # then
        np.testing.assert_array_almost_equal(120.232230, azimuth_sun, 5)

    def test_get_sun_elevation_from_arrays(self):
        # given
        latitude = -0.045
        longitude = 114.557
        altitude = 0
        coordinates = Coordinate(latitude, longitude, altitude)
        date = datetime(year=2020, month=1, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        subsolar_point = SubsolarPointCoordinate(date, date, frequency)
        longitudes = subsolar_point.get_selenographic_longitude_subsolar_point()
        latitudes = subsolar_point.get_selenographic_latitude_subsolar_point()
        lunar_sky = PositionSunLunarSky(date, date, frequency)

        # when
        elevation_sun = lunar_sky.get_sun_elevation_from_arrays(coordinates, longitudes, latitudes)

        # then
        np.testing.assert_array_almost_equal(89.987379, elevation_sun, 5)

    def test_get_sun_azimuth_from_arrays_error_dim(self):
        # given
        latitude = -0.045
        longitude = 114.557
        altitude = 0
        coordinates = Coordinate(latitude, longitude, altitude)
        date_1 = datetime(year=2020, month=1, day=1, hour=0, minute=0, second=0)
        date_2 = datetime(year=2020, month=1, day=2, hour=0, minute=0, second=0)
        frequency = 'H'
        subsolar_point_1 = SubsolarPointCoordinate(date_1, date_1, frequency)
        subsolar_point_2 = SubsolarPointCoordinate(date_1, date_2, frequency)
        longitudes = subsolar_point_1.get_selenographic_longitude_subsolar_point()
        latitudes = subsolar_point_2.get_selenographic_latitude_subsolar_point()
        lunar_sky = PositionSunLunarSky(date_1, date_1, frequency)

        # then
        with self.assertRaisesRegex(AssertionError, "The period for the selenographic longitudes and latitudes "
                                                    "must be the same"):
            lunar_sky.get_sun_azimuth_from_arrays(coordinates, longitudes, latitudes)

    def test_get_sun_elevation_from_arrays_error_dim(self):
        # given
        latitude = -0.045
        longitude = 114.557
        altitude = 0
        coordinates = Coordinate(latitude, longitude, altitude)
        date_1 = datetime(year=2020, month=1, day=1, hour=0, minute=0, second=0)
        date_2 = datetime(year=2020, month=1, day=2, hour=0, minute=0, second=0)
        frequency = 'H'
        subsolar_point_1 = SubsolarPointCoordinate(date_1, date_1, frequency)
        subsolar_point_2 = SubsolarPointCoordinate(date_1, date_2, frequency)
        longitudes = subsolar_point_1.get_selenographic_longitude_subsolar_point()
        latitudes = subsolar_point_2.get_selenographic_latitude_subsolar_point()
        lunar_sky = PositionSunLunarSky(date_1, date_1, frequency)

        # then
        with self.assertRaisesRegex(AssertionError, "The period for the selenographic longitudes and latitudes "
                                                    "must be the same"):
            lunar_sky.get_sun_elevation_from_arrays(coordinates, longitudes, latitudes)
