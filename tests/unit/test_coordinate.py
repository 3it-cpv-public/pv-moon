from unittest import TestCase

from cpv_moon.coordinate import Coordinate


class TestCoordinate(TestCase):
    def test_get_lat(self):
        # given
        coordinate = Coordinate(12, 24, 1.36)

        # when
        lat = coordinate.get_latitude()
        long = coordinate.get_longitude()
        alt = coordinate.get_altitude()

        # then
        self.assertEqual(12, lat)
        self.assertEqual(24, long)
        self.assertEqual(1.36, alt)
