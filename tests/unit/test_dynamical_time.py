from datetime import datetime
from unittest import TestCase

from cpv_moon.dynamical_time import get_dynamical_time_from_datetime


class Test(TestCase):
    def test_get_dynamical_time(self):
        """
        The truth is based on chapter 45 of MEEUS 1991.

        """
        # given
        timestamp = datetime(year=1992, month=4, day=12, hour=0, minute=0, second=0)

        # when
        dynamical_time = get_dynamical_time_from_datetime(timestamp)

        # then
        self.assertAlmostEqual(-0.077221081451, dynamical_time, 12)
