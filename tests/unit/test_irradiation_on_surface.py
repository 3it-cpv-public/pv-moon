from datetime import datetime
from unittest import TestCase

import numpy as np

from cpv_moon.coordinate import Coordinate
from cpv_moon.position_sun_lunar_sky import PositionSunLunarSky
from cpv_moon.irradiation_on_surface import get_surface_solar_irradiation, get_global_lunar_solar_irradiance, \
    get_global_irradiance_when_sun_visible, get_surface_solar_irradiation_from_arrays
from cpv_moon.type_tracker import TypeTracker


class TestIrradiationOnSurface(TestCase):
    def test_get_surface_solar_irradiation_fixed(self):
        """
        Test for a fixed surface. The test is based on the first result of the method.
        """
        # given
        latitude = 20
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=1, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=2, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        surface_azimuth_angle = 179
        surface_tilt_angle = 0
        type_tracker = TypeTracker.FIXED

        # when
        irradiation = get_surface_solar_irradiation(coordinates, date_begin, date_end, frequency, type_tracker,
                                                    surface_tilt_angle, surface_azimuth_angle)

        # then
        np.testing.assert_array_almost_equal(296061.19205409, irradiation, 8)

    def test_get_surface_solar_irradiation_vertical(self):
        """
        Test for a surface following the Sun on a vertical axis. The test is based on the first result of the method.
        """
        # given
        latitude = 40
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=5, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=6, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        surface_tilt_angle = 50
        type_tracker = TypeTracker.VERTICAL

        # when
        irradiation = get_surface_solar_irradiation(coordinates, date_begin, date_end, frequency, type_tracker,
                                                    surface_tilt_angle_in_degrees=surface_tilt_angle)

        # then
        np.testing.assert_array_almost_equal(459771.21026758, irradiation, 8)

    def test_get_surface_solar_irradiation_horizontal(self):
        """
        Test for a surface following the Sun on a horizontal axis. The test is based on the first result of the method.
        """
        # given
        latitude = 60
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=9, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=10, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        surface_azimuth_angle = 179
        type_tracker = TypeTracker.HORIZONTAL

        # when
        irradiation = get_surface_solar_irradiation(coordinates, date_begin, date_end, frequency, type_tracker,
                                                    surface_azimuth_angle_in_degrees=surface_azimuth_angle)

        # then
        np.testing.assert_array_almost_equal(384457.169383004, irradiation, 8)

    def test_get_surface_solar_irradiation_dual(self):
        """
        Test for a surface following the Sun on two axis. The test is based on the first result of the method.
        """
        # given
        latitude = 20
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=3, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=4, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        type_tracker = TypeTracker.DUAL

        # when
        irradiation = get_surface_solar_irradiation(coordinates, date_begin, date_end, frequency, type_tracker)

        # then
        np.testing.assert_array_almost_equal(498216.98002554, irradiation, 8)

    def test_get_surface_solar_irradiation_fixed_error_missing_azimuth(self):
        # given
        latitude = 20
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=1, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=2, day=1, hour=0, minute=0, second=0)
        frequency = 'H'

        surface_tilt_angle = 0
        type_tracker = TypeTracker.FIXED

        # then
        with self.assertRaisesRegex(AssertionError, "For a fixed surface, tilt and azimuth angles must be "
                                                    "given as the surface's orientation."):
            get_surface_solar_irradiation(coordinates, date_begin, date_end, frequency, type_tracker,
                                          surface_tilt_angle_in_degrees=surface_tilt_angle)

    def test_get_surface_solar_irradiation_fixed_error_missing_tilt(self):
        # given
        latitude = 20
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=1, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=2, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        surface_azimuth_angle = 0

        type_tracker = TypeTracker.FIXED

        # then
        with self.assertRaisesRegex(AssertionError, "For a fixed surface, tilt and azimuth angles must be "
                                                    "given as the surface's orientation."):
            get_surface_solar_irradiation(coordinates, date_begin, date_end, frequency, type_tracker,
                                          surface_azimuth_angle_in_degrees=surface_azimuth_angle)

    def test_get_surface_solar_irradiation_horizontal_error_missing_azimuth(self):
        # given
        latitude = 20
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=1, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=2, day=1, hour=0, minute=0, second=0)
        frequency = 'H'

        surface_tilt_angle = 0
        type_tracker = TypeTracker.HORIZONTAL

        # then
        with self.assertRaisesRegex(AssertionError, "For a one-axis horizontal tracked surface, the azimuth of"
                                                    "the horizontal axis must be given."):
            get_surface_solar_irradiation(coordinates, date_begin, date_end, frequency, type_tracker,
                                          surface_tilt_angle_in_degrees=surface_tilt_angle)

    def test_get_surface_solar_irradiation_vertical_error_missing_tilt(self):
        # given
        latitude = 20
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=1, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=2, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        surface_azimuth_angle = 0

        type_tracker = TypeTracker.VERTICAL

        # then
        with self.assertRaisesRegex(AssertionError, "For a one-axis vertical tracked surface, the tilt angle"
                                                    " must be given."):
            get_surface_solar_irradiation(coordinates, date_begin, date_end, frequency, type_tracker,
                                          surface_azimuth_angle_in_degrees=surface_azimuth_angle)

    def test_get_surface_solar_irradiation_from_arrays(self):
        """
        Test of the irradiation received using arrays as entry parameters
        """
        # given
        latitude = 20
        longitude = 20
        altitude = 1
        coordinates = Coordinate(latitude, longitude, altitude)
        date_begin = datetime(year=2022, month=3, day=1, hour=0, minute=0, second=0)
        date_end = datetime(year=2022, month=4, day=1, hour=0, minute=0, second=0)
        frequency = 'H'
        type_tracker = TypeTracker.DUAL
        lunar_sky = PositionSunLunarSky(date_begin, date_end, frequency)

        elevations_sun = lunar_sky.get_sun_elevation(coordinates)
        azimuths_sun = lunar_sky.get_sun_azimuth(coordinates)
        global_lunar_solar_irradiance = get_global_lunar_solar_irradiance(date_begin, date_end, frequency,)
        global_irradiance_when_sun_visible = get_global_irradiance_when_sun_visible(coordinates, elevations_sun,
                                                                                    global_lunar_solar_irradiance)

        # when
        irradiation = get_surface_solar_irradiation_from_arrays(type_tracker, azimuths_sun, elevations_sun,
                                                                global_irradiance_when_sun_visible)

        # then
        np.testing.assert_array_almost_equal(498216.98002554, irradiation, 8)
