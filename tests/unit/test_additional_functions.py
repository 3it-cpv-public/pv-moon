from unittest import TestCase

import numpy as np

from cpv_moon.additional_functions import min_max_elevation_csv, compute_illumination_fraction, \
    get_number_hours_partial_sun, get_losses_vs_irradiation_dual, get_irradiation_sun_fully_visible, \
    get_irradiation_sun_partially_visible, get_abs_filepath_from_module
from cpv_moon.coordinate import Coordinate
from cpv_moon.generation_files import get_boundaries_datetime_moon_year


class TestAdditionalFunctions(TestCase):
    def test_min_max_elevation_csv(self):
        # given
        selected_coordinates = Coordinate(0, 20, 1)
        csv_file_tmp = get_abs_filepath_from_module(__file__, "data/elevations_2023-01-22_2024-02-09.csv")

        # when
        min_max_elevations = min_max_elevation_csv(coordinates=selected_coordinates,
                                                   csv_file_path=csv_file_tmp)

        # then
        results = ["the minimum of the Sun's elevations is : -89.85004001604986",
                   "the maximum of the Sun's elevations is : 89.76000127242018"]
        np.testing.assert_equal(results, min_max_elevations)

    def test_get_losses_vs_irradiation_dual(self):
        # given
        selected_coordinates = Coordinate(0, 20, 1)
        file_path_dual = get_abs_filepath_from_module(__file__,
                                                      "data/maximum_irradiation_DUAL_2023-01-22_2024-02-09.csv")
        file_path_vertical = get_abs_filepath_from_module(__file__,
                                                          "data/maximum_irradiation_VERTICAL_2023-01-22_2024-02-09.csv")

        # when
        losses = get_losses_vs_irradiation_dual(coordinates=selected_coordinates, csv_file_path_irr_dual=file_path_dual,
                                                csv_file_path_irr_other_tracker=file_path_vertical)

        # then
        self.assertEqual(10.4, losses)

    def test_compute_illumination_fraction(self):
        # given
        selected_coordinates = Coordinate(0, 20, 1)
        csv_file_path_elevations = get_abs_filepath_from_module(__file__, "data/elevations_2023-01-22_2024-02-09.csv")

        # when
        illumination_fraction = compute_illumination_fraction(coordinates=selected_coordinates,
                                                              csv_file_path_elevations=csv_file_path_elevations)

        # then
        results = [9216, 4724, 51.25868055555556]
        np.testing.assert_equal(results, illumination_fraction)

    def test_get_number_hours_partial_sun(self):
        # given
        selected_coordinates = Coordinate(0, 20, 1)
        csv_file_path_elevations = get_abs_filepath_from_module(__file__, "data/elevations_2023-01-22_2024-02-09.csv")

        # when
        hours_partial_sun = get_number_hours_partial_sun(coordinates=selected_coordinates,
                                                         csv_file_path_elevations=csv_file_path_elevations)

        # then
        results = [26, 4724, 0.550381033022862]
        np.testing.assert_equal(results, hours_partial_sun)

    def test_get_irradiation_sun_fully_visible(self):
        # given
        selected_coordinates = Coordinate(0, 20, 1)
        csv_file_path_elev = get_abs_filepath_from_module(__file__, "data/elevations_2023-01-22_2024-02-09.csv")
        csv_file_path_lunar_irr = get_abs_filepath_from_module(__file__,
                                                               "data/lunar_global_irradiance_2023-01-22_2024-02-09.csv")

        # when
        irradiation_sun_fully_visible = get_irradiation_sun_fully_visible(coordinates=selected_coordinates,
                                                                          csv_file_path_elevations=csv_file_path_elev,
                                                                          csv_file_path_lunar_irradiance=
                                                                          csv_file_path_lunar_irr)

        # then
        results = [6416.893461363381, 1365.8777056967638]
        np.testing.assert_equal(results, irradiation_sun_fully_visible)

    def test_get_irradiation_sun_partially_visible(self):
        # given
        selected_coordinates = Coordinate(0, 20, 1)
        csv_file_path_elev = get_abs_filepath_from_module(__file__, "data/elevations_2023-01-22_2024-02-09.csv")
        csv_file_path_lunar_irr = get_abs_filepath_from_module(__file__,
                                                               "data/lunar_global_irradiance_2023-01-22_2024-02-09.csv")

        # when
        irradiation_sun_partially_visible = get_irradiation_sun_partially_visible(coordinates=selected_coordinates,
                                                                                  csv_file_path_elevations=
                                                                                  csv_file_path_elev,
                                                                                  csv_file_path_lunar_irradiance=
                                                                                  csv_file_path_lunar_irr)

        # then
        results = [35.50802443507982, -1.8860563890719593]
        np.testing.assert_equal(results, irradiation_sun_partially_visible)
