import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from cpv_moon.coordinate import Coordinate
from cpv_moon.generation_files import get_boundaries_datetime_moon_year, export_sun_elevations


def main() -> None:
    """
    This is a basic case study using functions of the package cpv_moon.
    :generated output: Plotting the elevations of the Sun at a point on the equator for the lunar year 2023.
    """
    print("start of the program")
    selected_coordinates = Coordinate(0, 20, 1)

    moon_year_start = 2023
    start_period, end_period = get_boundaries_datetime_moon_year(year_start=moon_year_start)
    frequency = 'H'

    export_sun_elevations(list_coordinates=[selected_coordinates], time_start=start_period, time_end=end_period,
                          frequency=frequency)

    elevations_sun = pd.read_csv('elevations_' + str(start_period.date()) + '_' + str(end_period.date()) + '.csv')[
        str(selected_coordinates.get_latitude())]

    _, axes = plt.subplots(1)

    plt.plot(np.arange(0, len(elevations_sun), 1), elevations_sun)

    plt.xticks(fontsize=11, fontweight='bold')
    plt.yticks(fontsize=11, fontweight='bold')
    axes.set_xticks(np.arange(0, len(elevations_sun), 500), minor=True)
    axes.set_xlim(xmin=0, xmax=len(elevations_sun))
    axes.set_ylim(ymin=-90, ymax=90)

    plt.xlabel("Hours in the period", fontsize=15, fontweight='bold')
    plt.ylabel("Elevation angle (°)", fontsize=15, fontweight='bold')

    axes.grid()
    axes.grid(which='minor', alpha=0.5)

    print("end of the example and graphic display")
    plt.show()



if __name__ == '__main__':
    main()
