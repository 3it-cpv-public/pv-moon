from cpv_moon.coordinate import Coordinate
from cpv_moon.generation_files import get_boundaries_datetime_moon_year, export_sun_elevations, export_sun_azimuths, \
    export_global_lunar_irradiance, export_irradiance_when_sun_visible, export_maximum_irradiation
from cpv_moon.type_tracker import TypeTracker


def main() -> None:
    """
    This is a basic case study using the main functions of the package cpv_moon.
    :generated output: The maximum irradiation received for the moon year 2023, for a point at the equator and with a
    fixed surface
    """
    print("Start of the example...")
    list_coordinates = [Coordinate(0, 20, 1), Coordinate(20, 20, 1), Coordinate(40, 20, 1), Coordinate(60, 20, 1),
                        Coordinate(80, 20, 1), Coordinate(89, 20, 1)]
    selected_coordinates = Coordinate(0, 20, 1)

    moon_year_start = 2023
    start_period, end_period = get_boundaries_datetime_moon_year(year_start=moon_year_start)
    frequency = 'H'

    export_sun_elevations(list_coordinates=list_coordinates, time_start=start_period,
                          time_end=end_period, frequency=frequency)
    export_sun_azimuths(list_coordinates=list_coordinates, time_start=start_period, time_end=end_period,
                        frequency=frequency)
    export_global_lunar_irradiance(time_start=start_period, time_end=end_period,
                                   frequency=frequency)

    export_irradiance_when_sun_visible(list_coordinates=list_coordinates,
                                       time_start=start_period, time_end=end_period,
                                       frequency=frequency)

    type_tracker = TypeTracker.FIXED
    export_maximum_irradiation(coordinates=selected_coordinates, time_start=start_period,
                               time_end=end_period, type_tracker=type_tracker)

    export_maximum_irradiation(coordinates=selected_coordinates, time_start=start_period,
                               time_end=end_period, type_tracker=TypeTracker.DUAL)

    print("End of the example, the exports have been done")


if __name__ == '__main__':
    main()
