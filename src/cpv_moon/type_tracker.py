from enum import Enum


class TypeTracker(Enum):
    """
    This enum the types of solar tracker.
    """
    FIXED = "fixed"
    VERTICAL = "vertical"
    HORIZONTAL = "horizontal"
    DUAL = "dual"
