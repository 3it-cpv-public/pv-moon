import csv
import os
from datetime import datetime

import numpy as np
import pandas as pd

from cpv_moon.calendar_date_boundaries import DateBoundaries
from cpv_moon.const import Const
from cpv_moon.coordinate import Coordinate
from cpv_moon.dynamical_time import get_dynamical_time_period_array
from cpv_moon.irradiation_on_surface import get_global_lunar_solar_irradiance, \
    get_surface_solar_irradiation_from_arrays, get_global_irradiance_when_sun_visible
from cpv_moon.position_sun_lunar_sky import PositionSunLunarSky
from cpv_moon.subsolar_point_coord import SubsolarPointCoordinate
from cpv_moon.type_tracker import TypeTracker


def get_boundaries_datetime_moon_year(year_start: int) -> (datetime, datetime):
    """
    Get the datetimes for the start and the end of the moon year that starts at the parameter year_start.
    :param year_start: the year start of the moon year period
    :return: the datetime of the start of the moon year, the datetime of the end of the moon year
    """
    date_boundaries = DateBoundaries.date_boundaries

    return date_boundaries[str(year_start)]["begin"], date_boundaries[str(year_start)]["end"]


def export_sun_elevations(list_coordinates: [Coordinate], time_start: datetime, time_end: datetime,
                          frequency: str) -> pd.DataFrame:
    """
    Export sun's elevations for given lunar positions and for the period between the time start
    and the time end at the frequency given.
    :param list_coordinates: list of the points coordinates
    :param time_start: start of the study period
    :param time_end: end of the study period
    :param frequency: the frequency of the computation in str format that is compatible with pandas.date_range function
    :return: the Sun's elevations at the listed coordinates for the period between the time start and the time end at
    the frequency given
    """
    lunar_sky = PositionSunLunarSky(time_start, time_end, frequency)

    period_time = len(get_dynamical_time_period_array(time_start, time_end, frequency))
    all_elevations_sun = np.zeros([period_time, len(list_coordinates)])

    subsolar_point_coordinates = SubsolarPointCoordinate(time_start, time_end, frequency)
    longitudes_subsolar_point = subsolar_point_coordinates.get_selenographic_longitude_subsolar_point()
    latitudes_subsolar_point = subsolar_point_coordinates.get_selenographic_latitude_subsolar_point()

    for index_coord, coordinates in enumerate(list_coordinates):
        elevations_sun = lunar_sky.get_sun_elevation_from_arrays(coordinates, longitudes_subsolar_point,
                                                                 latitudes_subsolar_point)
        all_elevations_sun[:, index_coord] = elevations_sun

    data_frame_elevations = pd.DataFrame(all_elevations_sun, columns=[str(coord.get_latitude()) for coord in
                                                                      list_coordinates])

    file_path_sun_elevations = create_file_path("elevations", time_start, time_end)

    export_csv(file_path_sun_elevations, data_frame_elevations, list_coordinates)

    print(f"The Sun's elevations for the period between {time_start.date()} and {time_end.date()} have been saved !")

    return data_frame_elevations


def export_sun_azimuths(list_coordinates: [Coordinate], time_start: datetime, time_end: datetime,
                        frequency: str) -> pd.DataFrame:
    """
    Export sun's azimuths for given lunar positions and for the period between the time start
    and the time end at the frequency given.
    :param list_coordinates: list of the points coordinates
    :param time_start: start of the study period
    :param time_end: end of the study period
    :param frequency: the frequency of the computation in str format that is compatible with pandas.date_range function
    :return: the Sun's azimuths at the listed coordinates for the period between the time start and the time end at the
    frequency given
    """
    lunar_sky = PositionSunLunarSky(time_start, time_end, frequency)

    period_time = len(get_dynamical_time_period_array(time_start, time_end, frequency))
    all_azimuths_sun = np.zeros([period_time, len(list_coordinates)])

    subsolar_point_coordinates = SubsolarPointCoordinate(time_start, time_end, frequency)
    longitudes_subsolar_point = subsolar_point_coordinates.get_selenographic_longitude_subsolar_point()
    latitudes_subsolar_point = subsolar_point_coordinates.get_selenographic_latitude_subsolar_point()

    for index_coord, coordinates in enumerate(list_coordinates):
        azimuths_sun = lunar_sky.get_sun_azimuth_from_arrays(coordinates, longitudes_subsolar_point,
                                                             latitudes_subsolar_point)
        all_azimuths_sun[:, index_coord] = azimuths_sun

    data_frame_azimuths = pd.DataFrame(all_azimuths_sun, columns=[str(coord.get_latitude()) for coord in
                                                                  list_coordinates])

    file_path_sun_azimuths = create_file_path("azimuths", time_start, time_end)

    export_csv(file_path_sun_azimuths, data_frame_azimuths, list_coordinates)

    print(f"The Sun's azimuths for the period between {time_start.date()} and {time_end.date()} have been saved !")

    return data_frame_azimuths


def export_global_lunar_irradiance(time_start: datetime, time_end: datetime, frequency: str) -> pd.DataFrame:
    """
    Export the global irradiance received by the Moon's surface for the period between the time start and the time end
    at the frequency given.
    :param time_start: the start time of the period
    :param time_end: the end time of the period
    :param frequency: the frequency of the computation in str format that is compatible with pandas.date_range function
    :return: a csv file with the global irradiance received by the Moon's surface for the period between the time start
    and the time end at the frequency given
    """
    lunar_global_irradiance = get_global_lunar_solar_irradiance(time_start, time_end, frequency)

    data_frame_lunar_global_irradiance = pd.DataFrame(lunar_global_irradiance, columns=["Lunar_irr"])

    file_path_lunar_global_irradiance = create_file_path("lunar_global_irradiance", time_start, time_end)
    data_frame_lunar_global_irradiance.to_csv(file_path_lunar_global_irradiance)

    print(f"The global lunar irradiance for the period between {time_start.date()} and {time_end.date()} has been "
          f"saved !")

    return data_frame_lunar_global_irradiance


def export_irradiance_when_sun_visible(list_coordinates: [Coordinate], time_start: datetime,
                                       time_end: datetime, frequency: str) -> pd.DataFrame:
    """
    Export the global irradiance received by a surface depending on its coordinates and for the chosen period.
    :param list_coordinates: list of the points coordinates
    :param time_start: the start time of the period
    :param time_end: the end time of the period
    :param frequency: the frequency of the computation in str format that is compatible with pandas.date_range function
    :return: a csv file with the global irradiance received by a surface depending on its coordinates and for the chosen
     period.
    """
    period_time = len(get_dynamical_time_period_array(time_start, time_end, frequency))
    all_irradiances_when_sun_visible = np.zeros([period_time, len(list_coordinates)])

    reader_lunar_global_irr = pd.read_csv('lunar_global_irradiance_' + str(time_start.date()) + '_' +
                                          str(time_end.date()) + '.csv')
    reader_elevations = pd.read_csv('elevations_' + str(time_start.date()) + '_' + str(time_end.date()) + '.csv')

    lunar_global_irr = reader_lunar_global_irr["Lunar_irr"]

    for index_coord, coordinates in enumerate(list_coordinates):
        elevations_sun = reader_elevations[str(coordinates.get_latitude())]
        irradiances_when_sun_visible = get_global_irradiance_when_sun_visible(coordinates, elevations_sun,
                                                                              lunar_global_irr)

        all_irradiances_when_sun_visible[:, index_coord] = irradiances_when_sun_visible

    data_frame_irradiances_when_sun_visible = pd.DataFrame(all_irradiances_when_sun_visible,
                                                           columns=[str(coord.get_latitude()) for coord in
                                                                    list_coordinates])

    file_path_irradiance_when_sun_visible = create_file_path("irradiance_when_sun_visible", time_start, time_end)

    export_csv(file_path_irradiance_when_sun_visible, data_frame_irradiances_when_sun_visible, list_coordinates)

    print(f"The irradiances when the sun is visible for the period between {time_start.date()} and {time_end.date()} "
          f"have been saved !")

    return data_frame_irradiances_when_sun_visible


def export_maximum_irradiation(coordinates: Coordinate, time_start: datetime, time_end: datetime,
                               type_tracker: TypeTracker) -> pd.DataFrame:
    """
    Export the maximum irradiation receive at the coordinates for the period chosen and for the type of tracker chosen.
    :param coordinates: point of interest coordinates
    :param time_start: the start time of the period
    :param time_end: the end time of the period
    :param type_tracker: type of tracker for the surface (fixed, one axis vertical or horizontal, two axis)
    :return: the maximum irradiation received during the period at the coordinates, depending on the the type of
    tracking
    """
    all_maximum_irradiation = np.zeros([3, 1])

    elevations_sun = pd.read_csv('elevations_' + str(time_start.date()) + '_' + str(time_end.date()) + '.csv')[
        str(coordinates.get_latitude())]
    azimuths_sun = pd.read_csv('azimuths_' + str(time_start.date()) + '_' + str(time_end.date()) + '.csv')[
        str(coordinates.get_latitude())]
    irradiances_when_sun_visible = pd.read_csv(
        'irradiance_when_sun_visible_' + str(time_start.date()) + '_' + str(time_end.date()) + '.csv')[
        str(coordinates.get_latitude())]
    maximum_irradiation = 0
    optimal_angles = [None, None]

    if type_tracker is TypeTracker.DUAL:
        maximum_irradiation = convert_to_kilo(get_surface_solar_irradiation_from_arrays(type_tracker, azimuths_sun,
                                                                                        elevations_sun,
                                                                                        irradiances_when_sun_visible))

    elif type_tracker is TypeTracker.FIXED:
        for azimuth in Const.RANGE_AZIMUTH_TILT_DEGREES[0]:
            for tilt in Const.RANGE_AZIMUTH_TILT_DEGREES[1]:
                irradiation = convert_to_kilo(
                    get_surface_solar_irradiation_from_arrays(type_tracker, azimuths_sun, elevations_sun,
                                                              irradiances_when_sun_visible, tilt,
                                                              azimuth))
                if irradiation > maximum_irradiation:
                    maximum_irradiation = irradiation
                    optimal_angles[0] = azimuth
                    optimal_angles[1] = tilt
        maximum_irradiation, optimal_angles = precised_maximum_irradiation(maximum_irradiation, optimal_angles,
                                                                           type_tracker, elevations_sun,
                                                                           azimuths_sun,
                                                                           irradiances_when_sun_visible)

    elif type_tracker is TypeTracker.HORIZONTAL:
        for azimuth in Const.RANGE_AZIMUTH_TILT_DEGREES[0]:
            irradiation = convert_to_kilo(
                get_surface_solar_irradiation_from_arrays(type_tracker, azimuths_sun, elevations_sun,
                                                          irradiances_when_sun_visible,
                                                          surface_azimuth_angle_in_degrees=azimuth))

            if irradiation > maximum_irradiation:
                maximum_irradiation = irradiation
                optimal_angles[0] = azimuth

        maximum_irradiation, optimal_angles = precised_maximum_irradiation(maximum_irradiation, optimal_angles,
                                                                           type_tracker, elevations_sun,
                                                                           azimuths_sun,
                                                                           irradiances_when_sun_visible)

    else:
        for tilt in Const.RANGE_AZIMUTH_TILT_DEGREES[1]:
            irradiation = convert_to_kilo(
                get_surface_solar_irradiation_from_arrays(type_tracker, azimuths_sun, elevations_sun,
                                                          irradiances_when_sun_visible,
                                                          surface_tilt_angle_in_degrees=tilt))

            if irradiation > maximum_irradiation:
                maximum_irradiation = irradiation
                optimal_angles[1] = tilt

        maximum_irradiation, optimal_angles = precised_maximum_irradiation(maximum_irradiation, optimal_angles,
                                                                           type_tracker, elevations_sun,
                                                                           azimuths_sun,
                                                                           irradiances_when_sun_visible)

    all_maximum_irradiation[0, 0] = maximum_irradiation
    all_maximum_irradiation[1, 0] = optimal_angles[0]
    all_maximum_irradiation[2, 0] = optimal_angles[1]

    data_frame_irradiation = pd.DataFrame(all_maximum_irradiation, columns=[str(coordinates.get_latitude())])

    export_csv(create_file_path("maximum_irradiation", time_start, time_end, type_tracker=type_tracker.name),
               data_frame_irradiation, [coordinates])

    print(
        f"The irradiation for the period between {time_start.date()} and {time_end.date()} and the type of tracker : "
        f"{type_tracker.name} have been saved !")

    return data_frame_irradiation


def precised_maximum_irradiation(maximum_irradiation: float, opti_angles: [float, float],
                                 type_tracker: TypeTracker, elevations_sun: np.ndarray, azimuths_sun: np.ndarray,
                                 irradiances_when_sun_visible: np.ndarray) -> (float, [float, float]):
    """

    :param maximum_irradiation: intermediate maximum irradiation
    :param opti_angles: intermediate optimum angles [tilt, azimuth]
    :param type_tracker: type of tracker for the surface (fixed, one axis vertical or horizontal, two axis)
    :param azimuths_sun: azimuths of the sun for a defined period and a defined place
    :param elevations_sun: elevations of the sun for a defined period and a defined place
    :param irradiances_when_sun_visible: irradiance when the Sun is visible in the sky, depending on the
    coordinates of the chosen point and a defined period
    :return: the maximum irradiation for optimum angles with precision of 0.1°
    """
    if type_tracker is TypeTracker.FIXED:
        for azimuth_around_opti in Const.RANGE_AZIMUTHS_AROUND_OPTI_DEGREES + opti_angles[0]:
            for tilt_around_opti in Const.RANGE_TILTS_AROUND_OPTI_DEGREES + opti_angles[1]:

                potential_max_irradiation = get_surface_solar_irradiation_from_arrays(type_tracker, azimuths_sun,
                                                                                      elevations_sun,
                                                                                      irradiances_when_sun_visible,
                                                                                      tilt_around_opti,
                                                                                      azimuth_around_opti)

                if convert_to_kilo(potential_max_irradiation) > maximum_irradiation \
                        and tilt_around_opti <= Const.MAXIMUM_BOUNDARY_TILT_IN_DEGREES:
                    opti_angles[1] = tilt_around_opti

                    if azimuth_around_opti < Const.MINIMUM_BOUNDARY_AZIMUTH_IN_DEGREES or \
                            azimuth_around_opti > Const.MAXIMUM_BOUNDARY_AZIMUTH_IN_DEGREES:
                        azimuth_around_opti = np.sign(azimuth_around_opti) * Const.MAXIMUM_BOUNDARY_AZIMUTH_IN_DEGREES \
                                              + (abs(azimuth_around_opti) % Const.MAXIMUM_BOUNDARY_AZIMUTH_IN_DEGREES)

                    opti_angles[0] = azimuth_around_opti
                    maximum_irradiation = convert_to_kilo(potential_max_irradiation)

    elif type_tracker is TypeTracker.VERTICAL:
        for tilt_around_opti in Const.RANGE_TILTS_AROUND_OPTI_DEGREES + opti_angles[1]:

            potential_max_irradiation = get_surface_solar_irradiation_from_arrays(type_tracker, azimuths_sun,
                                                                                  elevations_sun,
                                                                                  irradiances_when_sun_visible,
                                                                                  tilt_around_opti, None)

            if convert_to_kilo(
                    potential_max_irradiation) > maximum_irradiation \
                    and tilt_around_opti <= Const.MAXIMUM_BOUNDARY_TILT_IN_DEGREES:
                opti_angles[1] = tilt_around_opti

                maximum_irradiation = convert_to_kilo(potential_max_irradiation)

    else:
        for azimuth_around_opti in Const.RANGE_AZIMUTHS_AROUND_OPTI_DEGREES + opti_angles[0]:
            potential_max_irradiation = get_surface_solar_irradiation_from_arrays(type_tracker, azimuths_sun,
                                                                                  elevations_sun,
                                                                                  irradiances_when_sun_visible,
                                                                                  None, azimuth_around_opti)

            if convert_to_kilo(potential_max_irradiation) > maximum_irradiation:
                if azimuth_around_opti < Const.MINIMUM_BOUNDARY_AZIMUTH_IN_DEGREES \
                        or azimuth_around_opti > Const.MAXIMUM_BOUNDARY_AZIMUTH_IN_DEGREES:
                    azimuth_around_opti = np.sign(azimuth_around_opti) * Const.MAXIMUM_BOUNDARY_AZIMUTH_IN_DEGREES \
                                          + (abs(azimuth_around_opti) % Const.MAXIMUM_BOUNDARY_AZIMUTH_IN_DEGREES)

                opti_angles[0] = azimuth_around_opti
                maximum_irradiation = convert_to_kilo(potential_max_irradiation)

    return maximum_irradiation, opti_angles


def create_file_path(prefix: str, time_start: datetime, time_end: datetime, type_tracker="") -> str:
    """
    Create a file path according to a prefix and a period boundary. A type of tracking can be added.
    :param prefix: the prefix name of the csv file
    :param time_start: the start time of the period
    :param time_end: the end time of the period
    :param type_tracker: type of tracker to add.
    :return: the file path according to the prefix and the period boundary
    """
    if type_tracker == "":
        return prefix + "_" + str(time_start.date()) + '_' + str(time_end.date()) + '.csv'

    return prefix + "_" + type_tracker + "_" + str(time_start.date()) + '_' + str(time_end.date()) + '.csv'


def export_csv(file_path: str, data_frame: pd.DataFrame, list_coordinates: [Coordinate]) -> csv:
    """
    Modify a csv if it exists by adding new columns from the data frame or create a new one with the given data frame.
    :param file_path: file path of the csv
    :param data_frame: data_for_tests frame with the data_for_tests
    :param list_coordinates: list of the points coordinate
    :return: export the data_for_tests from a data_for_tests frame by modifying or creating a csv file
    """
    if os.path.exists(file_path) is True:
        updated_data_frame = pd.read_csv(file_path)

        for coord in list_coordinates:
            updated_data_frame[str(coord.get_latitude())] = data_frame[str(coord.get_latitude())]

        updated_data_frame.to_csv(file_path, index=False)

    else:
        data_frame.to_csv(file_path)


def convert_to_kilo(value_in_basic_unit: float) -> float:
    """
    Convert an value from the basic unit to the kilo value (* 10**(-3))
    :param value_in_basic_unit: the value desired to be converted
    :return: the converted value
    """
    return value_in_basic_unit * 10 ** (-3)
