class Coordinate:
    """
    Generic spherical coordinate
    """

    def __init__(self, latitude: float, longitude: float, altitude_in_km: float):
        """
        Construct a Coordinate instance
        :param latitude: Latitude
        :param longitude: Longitude
        :param altitude_in_km: Altitude in meter
        """
        self._latitude = latitude
        self._longitude = longitude
        self._altitude_in_km = altitude_in_km

    def get_latitude(self):
        """
        Get latitude in degree
        :return: Latitude
        """
        return self._latitude

    def get_longitude(self):
        """
        Get longitude in degree
        :return: Longitude
        """
        return self._longitude

    def get_altitude(self):
        """
        Get altitude in kilometer
        :return: Altitude
        """
        return self._altitude_in_km
