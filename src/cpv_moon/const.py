import numpy as np


class Const:
    """
    Some constants useful for the computation of the irradiation
    """

    # Boundaries of the tilt angle for a surface in degrees and for the elevation of the Sun
    MINIMUM_BOUNDARY_TILT_IN_DEGREES = 0
    MAXIMUM_BOUNDARY_TILT_IN_DEGREES = 90

    # Boundaries of the azimuth angles for a surface or the Sun.
    MINIMUM_BOUNDARY_AZIMUTH_IN_DEGREES = -180
    MAXIMUM_BOUNDARY_AZIMUTH_IN_DEGREES = 180

    # Range of angles around the optimum to be more precised at 0.1°
    RANGE_AZIMUTHS_AROUND_OPTI_DEGREES = np.arange(-2, 2.1, 0.1)
    RANGE_TILTS_AROUND_OPTI_DEGREES = np.arange(-2, 2.1, 0.1)

    # Range of all the azimuths and tilts angles of a surface. [0] is for azimuths, [1] is for tilts.
    RANGE_AZIMUTH_TILT_DEGREES = [np.arange(MINIMUM_BOUNDARY_AZIMUTH_IN_DEGREES, MAXIMUM_BOUNDARY_AZIMUTH_IN_DEGREES+2
                                            , 2), np.arange(MINIMUM_BOUNDARY_TILT_IN_DEGREES,
                                                            MAXIMUM_BOUNDARY_TILT_IN_DEGREES+2, 2)]
