# This module computes energy production of a module
import copy
from datetime import datetime

import numpy as np

from cpv_moon.celest_objects.astronomical_constants import AstronomicalConstants
from cpv_moon.coordinate import Coordinate
from cpv_moon.earth_moon_sun_system import EarthMoonSunSystem
from cpv_moon.position_sun_lunar_sky import PositionSunLunarSky
from cpv_moon.type_tracker import TypeTracker


def get_global_lunar_solar_irradiance(begin: datetime, end: datetime, frequency: str) -> np.ndarray:
    """
    Get the global lunar solar irradiance (in W/m²) for a given period of time. From LI and al (2012)
    :param begin: beginning of the period in the format datetime(year, month, day, hour)
    :param end: end of the period in the format datetime(year, month, day, hour)
    :param frequency: the frequency of the computation in str format that is compatible with pandas.date_range function
    :return: the global lunar solar irradiance (in W/m²) for the given period = begin -> end
    """
    earth_moon_sun_system = EarthMoonSunSystem(begin, end, frequency)

    # Ecliptical latitude in degrees and distance Earth-Moon in km
    ecliptical_latitude = earth_moon_sun_system.get_heliocentric_ecliptical_lat_moon()
    earth_moon_distance_in_km = earth_moon_sun_system.get_earth_moon_distance()

    # Distance Earth-Moon in AU
    earth_moon_distance = earth_moon_distance_in_km / AstronomicalConstants.AU_IN_KM

    # Distance Earth-Sun in AU
    sun_earth_distance = earth_moon_sun_system.get_sun_earth_distance()

    solar_constant = AstronomicalConstants.SOLAR_CONSTANT_IN_W_M2

    return solar_constant / ((earth_moon_distance * np.sin(np.radians(ecliptical_latitude)
                                                           * sun_earth_distance / earth_moon_distance)
                              / (np.sin(np.radians(ecliptical_latitude)))) ** 2)


def get_sun_incidence_angle_on_surface(azimuth_sun_in_degrees: np.ndarray, elevation_sun_in_degrees: np.ndarray,
                                       type_tracker: TypeTracker, azimuth_surface_in_degrees=None,
                                       tilt_surface_in_degrees=None) -> np.ndarray:
    """
    Get the Sun's angle of incidence (in degrees) on a surface with a given tracked mode.
    Adapted from BRAUN and MITCHELL (1983)
    :param azimuth_sun_in_degrees: the Sun's azimuth angle gotten from position_sun_lunar_sky.py
    :param elevation_sun_in_degrees: the Sun's elevation angle gotten from position_sun_lunar_sky.py
    :param type_tracker: tracker mode used, can be fixed, one-axis vertical or horizontal, two-axis
    :param azimuth_surface_in_degrees: the surface's azimuth angle, equals to zero when facing the North, positive
    toward east, negative toward west (Between -180° and 180°)
    :param tilt_surface_in_degrees: the surface's tilt angle, equals to zero when horizontal (Between 0° and 90°)
    :return: the Sun's angle of incidence on the surface in degrees depending on the tracker mode
    """
    if type_tracker is TypeTracker.FIXED:
        assert azimuth_surface_in_degrees is not None, "For a fixed surface, tilt and azimuth angles must be " \
                                                       "given as the surface's " \
                                                       "orientation."
        assert tilt_surface_in_degrees is not None, "For a fixed surface, tilt and azimuth angles must be " \
                                                    "given as the surface's " \
                                                    "orientation."
        return np.degrees(
            np.arccos(
                np.cos(np.radians(90 - elevation_sun_in_degrees)) * np.cos(
                    np.radians(tilt_surface_in_degrees)) + np.sin(np.radians(tilt_surface_in_degrees)) * np.cos(
                    np.radians(azimuth_sun_in_degrees - azimuth_surface_in_degrees)) * np.sin(
                    np.radians(90 - elevation_sun_in_degrees))))

    if type_tracker is TypeTracker.VERTICAL:
        assert tilt_surface_in_degrees is not None, "For a one-axis vertical tracked surface, the tilt angle" \
                                                    " must be given."

        return np.degrees(
            np.arccos(
                np.cos(np.radians(90 - elevation_sun_in_degrees)) * np.cos(
                    np.radians(tilt_surface_in_degrees)) + np.sin(np.radians(tilt_surface_in_degrees)) * np.cos(
                    np.radians(azimuth_sun_in_degrees - azimuth_sun_in_degrees)) * np.sin(
                    np.radians(90 - elevation_sun_in_degrees))))

    if type_tracker is TypeTracker.HORIZONTAL:
        assert azimuth_surface_in_degrees is not None, "For a one-axis horizontal tracked surface, the azimuth of" \
                                                       "the horizontal axis must be given."

        # Term for one axis tracking around an horizontal axis
        rotation_angle = np.degrees(np.arctan(
            np.tan(np.radians(90 - elevation_sun_in_degrees)) * np.sin(
                np.radians(azimuth_sun_in_degrees - azimuth_surface_in_degrees))))

        azimuth_surface_modulo_pi = azimuth_surface_in_degrees + np.degrees(np.arcsin(np.sign(rotation_angle)))

        return np.degrees(
            np.arccos(
                np.cos(np.radians(90 - elevation_sun_in_degrees)) * np.cos(np.radians(abs(rotation_angle)))
                + np.sin(np.radians(abs(rotation_angle))) * np.cos(
                    np.radians(azimuth_sun_in_degrees - azimuth_surface_modulo_pi)) *
                np.sin(np.radians(90 - elevation_sun_in_degrees))))

    return np.zeros([len(azimuth_sun_in_degrees.ravel())])


def get_global_irradiance_when_sun_visible(coordinates: Coordinate, elevations_sun: np.ndarray,
                                           global_lunar_solar_irradiance: np.ndarray) -> np.array([1, ]):
    """
    Compute the global direct normal irradiance available for a surface when the Sun is visible in the sky at a defined
    location and for a defined period.
    :param coordinates: the selenographic coordinates of the surface
    :param elevations_sun: the elevations of the Sun for a defined period
    :param global_lunar_solar_irradiance: the global lunar irradiance for a defined period
    :return: an array of the irradiance received hour by hour by the Moon for the defined period of time (in W/m²)
    """
    # Initialising the irradiance on the surface as the global lunar irradiance itself
    global_irradiance_when_sun_visible = copy.deepcopy(global_lunar_solar_irradiance)

    # Get the limit angle where the sun is still visible. Depends on the altitude (topography not counted)
    limit_angle = get_angle_sun_visible_beyond_zero(coordinates)

    for index_irradiance, _ in enumerate(global_irradiance_when_sun_visible):
        if -(limit_angle + AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES) \
                < elevations_sun[index_irradiance] < - (limit_angle
                                                        - AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES):
            # The following formula is based on the considerations:
            # If the Sun elevation is at the limit angle + the semi apparent diameter, it is fully visible
            # If the Sun elevation is at the limit angle, it is half visible
            # If the Sun elevation is at the limit angle - the semi apparent diameter, it is not visible
            global_irradiance_when_sun_visible[index_irradiance] *= (
                    1 + 1 / (2 * AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES) * (
                    elevations_sun[index_irradiance] + limit_angle
                    - AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES))

        elif elevations_sun[index_irradiance] < - (
                limit_angle + AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES):
            global_irradiance_when_sun_visible[index_irradiance] = 0

    return global_irradiance_when_sun_visible


def get_irradiance_on_an_oriented_surface(incidence_angle_sun: np.ndarray,
                                          irradiance_on_surface_facing_sun: np.ndarray) -> np.ndarray:
    """
    Cancel the irradiance received by a surface if the Sun is behind the surface.
    :param incidence_angle_sun: incidence angles of the Sun on the surface for a defined period
    :param irradiance_on_surface_facing_sun: irradiance received by a surface facing the Sun for a defined period of
    time
    :return: the irradiance on a surface with an oriented surface
    """
    irradiance_oriented_surface = copy.deepcopy(irradiance_on_surface_facing_sun)
    # Get the incidence angle as one dimension array
    incidence_angle_sun_ravel = incidence_angle_sun.ravel()

    # If the sun is behind the surface, incidence angle > 90° and so the surface should not receive irradiance
    for index_irradiance, _ in enumerate(incidence_angle_sun_ravel):
        if abs(incidence_angle_sun_ravel[index_irradiance]) > 90:
            irradiance_oriented_surface[index_irradiance] = 0

    return irradiance_oriented_surface


def get_surface_solar_irradiation(coordinates: Coordinate, begin: datetime, end: datetime, frequency: str,
                                  type_tracker: TypeTracker, surface_tilt_angle_in_degrees=None,
                                  surface_azimuth_angle_in_degrees=None) -> float:
    """
    Get the solar irradiation in Wh/m^2 received by an oriented and localised surface for a given period.
    :param begin: beginning of the period
    :param end: end of the period
    :param frequency: the frequency of the computation in str format that is compatible with pandas.date_range function
    :param type_tracker: type of tracker for the surface (fixed, one axis vertical or horizontal, two axis)
    :param coordinates: the selenographic coordinates of the surface
    :param surface_tilt_angle_in_degrees: the surface's tilt angle in degrees (for fixed and vertical tracking only)
    :param surface_azimuth_angle_in_degrees: the surface's azimuth angle in degrees (for fixed and horizontal only)
    :return: the solar irradiation received by the given surface and for the given period between the beginning and the
     end
    """
    # Define a lunar sky for the given timestamp
    lunar_sky = PositionSunLunarSky(begin, end, frequency)

    # Get the azimuth and elevation angles of the Sun in the lunar sky for the position of the surface
    azimuth_sun = lunar_sky.get_sun_azimuth(coordinates)
    elevation_sun = lunar_sky.get_sun_elevation(coordinates)

    # Get the global lunar solar irradiance for the given period
    global_lunar_solar_irradiance = get_global_lunar_solar_irradiance(begin, end, frequency)

    # Initialising the irradiance on the surface as if the surface is facing the Sun
    global_irradiance_when_sun_visible = get_global_irradiance_when_sun_visible(coordinates, elevation_sun,
                                                                                global_lunar_solar_irradiance)

    # Get the incidence angle of the Sun on the surface depending on the type of tracker
    incidence_angle_sun = get_sun_incidence_angle_on_surface(azimuth_sun, elevation_sun, type_tracker,
                                                             surface_azimuth_angle_in_degrees,
                                                             surface_tilt_angle_in_degrees)

    # Get the irradiance for the oriented surface
    irradiance_on_oriented_surface = get_irradiance_on_an_oriented_surface(incidence_angle_sun,
                                                                           global_irradiance_when_sun_visible)

    # Get the final solar irradiation for the period (in Wh/m^2)
    return float(np.dot(irradiance_on_oriented_surface, np.cos(np.radians(incidence_angle_sun.ravel()))))


def get_angle_sun_visible_beyond_zero(coordinates: Coordinate) -> float:
    """
    Get the angle which represents how many degrees below an elevation of zero the Sun is still visible. From
    TSCHUDIN (2016).
    :param coordinates: selenographic coordinates of the point
    :return: the number of degrees below horizon for which the Sun is still visible
    """
    # Get the altitude of the point
    altitude_point = coordinates.get_altitude()

    # Distance for the limit of vision depending on the altitude of the point
    if altitude_point == 0:
        return 0

    ratio_pythagore = AstronomicalConstants.MOON_RADIUS_IN_KM/(AstronomicalConstants.MOON_RADIUS_IN_KM + altitude_point)
    # Computation of the angle thanks to trigonometry
    return float(np.degrees(np.arccos(ratio_pythagore)))


def get_surface_solar_irradiation_from_arrays(type_tracker: TypeTracker, azimuths_sun, elevations_sun,
                                              global_irradiance_when_sun_visible, surface_tilt_angle_in_degrees=None,
                                              surface_azimuth_angle_in_degrees=None) -> float:
    """
    Get the solar irradiation in Wh/m^2 received by an oriented and localised surface for a given period.

    :param type_tracker: type of tracker for the surface (fixed, one axis vertical or horizontal, two axis)
    :param azimuths_sun: azimuths of the sun for a defined period and a defined place
    :param elevations_sun: elevations of the sun for a defined period and a defined place
    :param global_irradiance_when_sun_visible: global irradiance when the Sun is visible in the sky, depending on the
    coordinates of the chosen point and a defined period
    :param surface_tilt_angle_in_degrees: the surface's tilt angle in degrees (for fixed and vertical tracking only)
    :param surface_azimuth_angle_in_degrees: the surface's azimuth angle in degrees (for fixed and horizontal only)
    :return: the solar irradiation received by the given surface and for the given period between the beginning and the
     end
    """
    assert (len(azimuths_sun) == len(elevations_sun)) is True, "The period for the azimuths and the elevations " \
                                                               "must be the same"

    assert (len(azimuths_sun) == len(global_irradiance_when_sun_visible)) is True, "The period for the positions of " \
                                                                                   "the Sun in the sky and the " \
                                                                                   "irradiance must be the same"

    incidence_angle_sun = get_sun_incidence_angle_on_surface(azimuths_sun, elevations_sun, type_tracker,
                                                             surface_azimuth_angle_in_degrees,
                                                             surface_tilt_angle_in_degrees)

    irradiance_on_oriented_surface = get_irradiance_on_an_oriented_surface(incidence_angle_sun,
                                                                           global_irradiance_when_sun_visible)

    # Get the final solar irradiation for the period (in Wh/m^2)
    return float(np.dot(irradiance_on_oriented_surface, np.cos(np.radians(incidence_angle_sun.ravel()))))
