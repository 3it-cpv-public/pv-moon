"""The objective is to get the altitude and azimuth of the Sun for a position on the Moon at a certain time"""
from datetime import datetime

import numpy as np

from cpv_moon.celest_objects.astronomical_constants import AstronomicalConstants
from cpv_moon.coordinate import Coordinate
from cpv_moon.subsolar_point_coord import SubsolarPointCoordinate


class PositionSunLunarSky:
    """ This class computes the azimuth and the elevation of the Sun in the lunar sky. Each instance is created for
    a fixed period of time with an interval equals to one hour.
    The azimuth is the angle between the north vector and the Sun's vector on the horizontal plane. (wikipedia)
    The elevation is the vertical angular distance between the Sun and the observer's local horizon or, also called,
    the observer’s local plane. (https://www.photopills.com/articles/understanding-azimuth-and-elevation)
    """

    def __init__(self, begin: datetime, end: datetime, frequency: str):
        self._subsolar_point = SubsolarPointCoordinate(begin, end, frequency)

    def get_sun_azimuth(self, coordinates: Coordinate) -> np.ndarray:
        """
        Compute the azimuth angle of the Sun from a position of a point in the Moon's surface.
        From LI et al 2008 and ZHANG, STACKHOUSE, MACPHERSON et al 2021
        :param coordinates: the selenographic coordinates of the chosen point
        :return: the azimuth of the Sun at the moment of the system
        """
        # Creation of the useful terms from coordinates.py
        longitude = coordinates.get_longitude()
        latitude = coordinates.get_latitude()

        # Selenographic coordinates of the subsolar point
        selenographic_longitude_sun = self._subsolar_point.get_selenographic_longitude_subsolar_point()
        selenographic_latitude_sun = self._subsolar_point.get_selenographic_latitude_subsolar_point()

        # Arguments for azimuth
        component_x_vector_pointing_sun = np.cos(np.radians(selenographic_latitude_sun)) * np.sin(
            np.radians(selenographic_longitude_sun) - np.radians(longitude))
        component_y_vector_pointing_sun = np.cos(np.radians(latitude)) * np.sin(
            np.radians(selenographic_latitude_sun)) \
                                          - np.sin(np.radians(latitude)) * np.cos(
            np.radians(selenographic_latitude_sun)) * np.cos(
            np.radians(selenographic_longitude_sun) - np.radians(longitude))

        return np.degrees(np.arctan2(component_x_vector_pointing_sun, component_y_vector_pointing_sun))

    def get_sun_elevation(self, coordinates: Coordinate) -> np.ndarray:
        """
        Compute the elevation angle of the Sun from a position of a point in the Moon's surface.
        From LI et al 2008 and ZHANG, STACKHOUSE, MACPHERSON et al 2021
        :param coordinates: the selenographic coordinates of the chosen point
        :return: the elevation of the Sun at the moment of the system
        """
        # Creation of the useful terms from coordinates.py
        longitude = coordinates.get_longitude()
        latitude = coordinates.get_latitude()
        altitude_astronomical_unit = coordinates.get_altitude() / AstronomicalConstants.AU_IN_KM

        # Ecliptical latitude in degrees and distance Earth-Moon in AU
        ecliptical_latitude_moon = self._subsolar_point.earth_moon_sun_system.get_heliocentric_ecliptical_lat_moon()
        earth_moon_distance_in_km = self._subsolar_point.earth_moon_sun_system.get_earth_moon_distance()
        earth_moon_distance = earth_moon_distance_in_km / AstronomicalConstants.AU_IN_KM

        # Distance Earth-Sun in AU
        sun_earth_distance = self._subsolar_point.earth_moon_sun_system.get_sun_earth_distance()

        # Selenographic coordinates of the subsolar point
        selenographic_longitude_sun = self._subsolar_point.get_selenographic_longitude_subsolar_point()
        selenographic_latitude_sun = self._subsolar_point.get_selenographic_latitude_subsolar_point()

        # Arguments for the computation of the elevation angle
        radius_moon = self._subsolar_point.earth_moon_sun_system.moon.get_radius()

        alpha = np.arccos(-np.sin(np.radians(-latitude)) * np.sin(np.radians(selenographic_latitude_sun))
                          + np.cos(np.radians(-latitude)) * np.cos(np.radians(selenographic_latitude_sun))
                          * np.cos(-1 * np.radians(selenographic_longitude_sun) + np.radians(longitude)))

        beta = np.arcsin((radius_moon + altitude_astronomical_unit) * np.sin(alpha) / (np.sqrt(
            earth_moon_distance ** 2 * np.sin(np.radians(ecliptical_latitude_moon)) ** 2 / (np.sin(
                np.radians(ecliptical_latitude_moon) * earth_moon_distance / sun_earth_distance) ** 2) + (
                        radius_moon + altitude_astronomical_unit) ** 2 - 2 * earth_moon_distance * (
                        radius_moon + altitude_astronomical_unit) * np.sin(
                np.radians(ecliptical_latitude_moon)) * np.cos(alpha) / (
                np.sin(np.radians(ecliptical_latitude_moon) * earth_moon_distance / sun_earth_distance)))))

        # NB: alpha + beta equals to the zenith angle of the Sun
        return 90 - (np.degrees(alpha) + np.degrees(beta))

    def get_sun_elevation_from_arrays(self, coordinates: Coordinate, selenographic_longitude_sun: np.ndarray,
                                      selenographic_latitude_sun: np.ndarray) -> np.ndarray:
        """
        Compute the elevation angle of the Sun from a position of a point in the Moon's surface. The selenographic
        longitudes and latitudes of the Sun are given by arrays.
        From LI et al 2008 and ZHANG, STACKHOUSE, MACPHERSON et al 2021
        :param coordinates: the selenographic coordinates of the chosen point
        :param selenographic_longitude_sun: arrays with selenographic longitudes of the subsolar point for a defined
        period
        :param selenographic_latitude_sun: arrays with selenographic latitudes of the subsolar point for a defined
        period
        :return: the elevations of the Sun for the same period than the selenographic coordinates of the subsolar point
        """
        assert (len(selenographic_longitude_sun) == len(selenographic_latitude_sun)) is True, "The period for the " \
                                                                                       "selenographic longitudes and " \
                                                                                       "latitudes must be the same"
        # Creation of the useful terms from coordinates.py
        longitude = coordinates.get_longitude()
        latitude = coordinates.get_latitude()
        altitude_astronomical_unit = coordinates.get_altitude() / AstronomicalConstants.AU_IN_KM

        # Ecliptical latitude in degrees and distance Earth-Moon in AU
        ecliptical_latitude_moon = self._subsolar_point.earth_moon_sun_system.get_heliocentric_ecliptical_lat_moon()
        earth_moon_distance_in_km = self._subsolar_point.earth_moon_sun_system.get_earth_moon_distance()
        earth_moon_distance = earth_moon_distance_in_km / AstronomicalConstants.AU_IN_KM

        # Distance Earth-Sun in AU
        sun_earth_distance = self._subsolar_point.earth_moon_sun_system.get_sun_earth_distance()

        # Arguments for the computation of the elevation angle
        radius_moon = self._subsolar_point.earth_moon_sun_system.moon.get_radius()

        alpha = np.arccos(-np.sin(np.radians(-latitude)) * np.sin(np.radians(selenographic_latitude_sun))
                          + np.cos(np.radians(-latitude)) * np.cos(np.radians(selenographic_latitude_sun))
                          * np.cos(-1 * np.radians(selenographic_longitude_sun) + np.radians(longitude)))

        beta = np.arcsin((radius_moon + altitude_astronomical_unit) * np.sin(alpha) / (np.sqrt(
            earth_moon_distance ** 2 * np.sin(np.radians(ecliptical_latitude_moon)) ** 2 / (np.sin(
                np.radians(ecliptical_latitude_moon) * earth_moon_distance / sun_earth_distance) ** 2) + (
                    radius_moon + altitude_astronomical_unit) ** 2 - 2 * earth_moon_distance * (
                    radius_moon + altitude_astronomical_unit) * np.sin(
                np.radians(ecliptical_latitude_moon)) * np.cos(alpha) / (
                np.sin(np.radians(ecliptical_latitude_moon) * earth_moon_distance / sun_earth_distance)))))

        # NB: alpha + beta equals to the zenith angle of the Sun
        return 90 - (np.degrees(alpha) + np.degrees(beta))

    @staticmethod
    def get_sun_azimuth_from_arrays(coordinates: Coordinate, selenographic_longitude_sun: np.ndarray,
                                    selenographic_latitude_sun: np.ndarray) -> np.ndarray:
        """
        Get the Sun azimuth angles at a chosen point, from selenographic longitudes and latitudes of the subsolar point.
        :param coordinates: Coordinates of the point
        :param selenographic_longitude_sun: Selenographic longitudes of the subsolar point
        :param selenographic_latitude_sun: Selenographic latitudes of the subsolar point
        :return: Sun azimuth angles at the chosen point anf for the period corresponding to the computation of
        longitudes and latitudes
        """
        assert (len(selenographic_longitude_sun) == len(selenographic_latitude_sun)) is True, "The period for the " \
                                                                                              "selenographic " \
                                                                                              "longitudes and " \
                                                                                              "latitudes must be " \
                                                                                              "the same"

        # Creation of the useful terms from coordinates.py
        longitude = coordinates.get_longitude()
        latitude = coordinates.get_latitude()

        # Arguments for azimuth
        component_x_vector_pointing_sun = np.cos(np.radians(selenographic_latitude_sun)) * np.sin(
            np.radians(selenographic_longitude_sun) - np.radians(longitude))
        component_y_vector_pointing_sun = np.cos(np.radians(latitude)) * np.sin(
            np.radians(selenographic_latitude_sun)) \
                                          - np.sin(np.radians(latitude)) * np.cos(
            np.radians(selenographic_latitude_sun)) * np.cos(
            np.radians(selenographic_longitude_sun) - np.radians(longitude))

        return np.degrees(np.arctan2(component_x_vector_pointing_sun, component_y_vector_pointing_sun))
