import os

import numpy as np
import pandas as pd

from cpv_moon.celest_objects.astronomical_constants import AstronomicalConstants
from cpv_moon.coordinate import Coordinate
from cpv_moon.irradiation_on_surface import get_angle_sun_visible_beyond_zero


def min_max_elevation_csv(coordinates: Coordinate, csv_file_path: os.path) -> [float, float]:
    """
    Compute the minimum and the maximum of the Sun's elevation at a given position and for a given period.
    :param csv_file_path: file path of the csv that we want to study. The csv must exist and contain the elevations of
    the Sun at the coordinates defined.
    :param coordinates: coordinates of the considered point
    :return: min and max Sun's elevation at the given position and for the given lunar year
    """
    assert os.path.exists(csv_file_path) is True, "The file does not exist !"
    reader_elevations = pd.read_csv(csv_file_path)

    elevations_sun = reader_elevations[str(coordinates.get_latitude())]
    min_elevations = min(elevations_sun)
    max_elevations = max(elevations_sun)

    return [f"the minimum of the Sun's elevations is : {min_elevations}",
            f"the maximum of the Sun's elevations is : {max_elevations}"]


def get_losses_vs_irradiation_dual(coordinates: Coordinate, csv_file_path_irr_dual: str,
                                   csv_file_path_irr_other_tracker: str) -> float:
    """
    Get the percentage of loss of a tracker compared to the dual axis tracker, at a chosen point and for a chosen moon
    year.
    :param coordinates: coordinates of the studied point
    :param csv_file_path_irr_dual: file path of the csv with the irradiations coming from a two-axis tracker
    :param csv_file_path_irr_other_tracker: file path of the csv with the irradiations coming from another tracker
    :return: the percentage of loss irradiation of a tracker compared to the dual axis tracker, at a chosen point and
    for a chosen moon year
    """

    reader_irradiation_dual = pd.read_csv(csv_file_path_irr_dual)
    irradiation_dual = reader_irradiation_dual[str(coordinates.get_latitude())][0]

    reader_irradiation_tracker = pd.read_csv(csv_file_path_irr_other_tracker)
    irradiation_tracker = reader_irradiation_tracker[str(coordinates.get_latitude())][0]

    irradiation_loose = (irradiation_dual - irradiation_tracker) / irradiation_dual * 100

    if irradiation_loose < 0.1:
        return float(f'{irradiation_loose:.2f}')

    return float(f'{irradiation_loose:.1f}')


def compute_illumination_fraction(coordinates: Coordinate, csv_file_path_elevations: str) -> [float, float, float]:
    """
    Compute the illumination fraction (portion of time when the Sun is visible in the sky) of a location for a defined
    period.
    :param coordinates: coordinates of the studied point
    :param csv_file_path_elevations: file path of the elevations
    :return: the total numbers of Sun hours [0], the number of hours when the Sun is visible [1], the illumination
    fraction (portion of time when the Sun is visible in the sky) [2] of a location for a defined period
    """
    limit_angle = get_angle_sun_visible_beyond_zero(coordinates)
    hours_sun_visible = 0

    reader_elevations = pd.read_csv(csv_file_path_elevations)

    elevations_sun = reader_elevations[str(coordinates.get_latitude())]
    total_number_hours_sun = len(elevations_sun)

    for _, elev in enumerate(elevations_sun):
        if elev > - limit_angle - AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES:
            hours_sun_visible += 1

    print(
        f"The latitude of the location is {coordinates.get_latitude()}, the total number of Sun hours is "
        f"{total_number_hours_sun}, the number of hours when the Sun is visible is {hours_sun_visible} which represent "
        f"{float(f'{100 * hours_sun_visible / total_number_hours_sun:.1f}')}" + "%")

    return [total_number_hours_sun, hours_sun_visible, 100 * hours_sun_visible / total_number_hours_sun]


def get_number_hours_partial_sun(coordinates: Coordinate, csv_file_path_elevations: str) -> [float, float, float]:
    """
    Compute the number of hours when the Sun is partially visible when it is visible in the sky at a defined location
    and for a defined period.
    :param coordinates: coordinates of the studied point
    :param csv_file_path_elevations: file path of the elevations
    :return: the number of hours when the Sun is partially visible when it is visible in the sky [0], the total number
    of hours for the period selected [1], and the ratio between the two [2]
    """
    count_hours_partially_visible = 0
    count_total_hours = 0
    limit_angle = get_angle_sun_visible_beyond_zero(coordinates)

    reader_elevations = pd.read_csv(csv_file_path_elevations)

    elevations_sun = reader_elevations[str(coordinates.get_latitude())]
    for elev in elevations_sun:
        if elev > -limit_angle - AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES:
            count_total_hours += 1
        if -limit_angle - AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES < elev \
                < -limit_angle + AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES:
            count_hours_partially_visible += 1

    print(f"The number of hours when the Sun is partially visible is {count_hours_partially_visible} over a total"
          f"of {count_total_hours} hours when the Sun is visible, which is a fraction of "
          f"{count_hours_partially_visible / count_total_hours * 100}")

    return [count_hours_partially_visible, count_total_hours, count_hours_partially_visible / count_total_hours * 100]


def get_irradiation_sun_fully_visible(coordinates: Coordinate, csv_file_path_elevations: str,
                                      csv_file_path_lunar_irradiance: str) -> [float, float, float, float]:
    """
    Get the irradiation when the Sun is fully visible, at a chose coordinate and for a chosen period.
    This function also computes the mean irradiance during this period.
    :param csv_file_path_elevations: file path of the elevations
    :param csv_file_path_lunar_irradiance: file path of the lunar irradiance
    :param coordinates: coordinates of the studied point
    :return: the irradiation when the Sun is fully visible and when it is partially visible, at a chose coordinate and
    for a chosen moon year. This function also computes the mean irradiance during this moon year when the Sun is
    partially visible and fully visible
    """
    limit_angle = get_angle_sun_visible_beyond_zero(coordinates)
    total_irr_fully_visible = 0
    irradiance_sun_fully_visible = []

    reader_elev = pd.read_csv(csv_file_path_elevations)

    reader_lunar_irradiance = pd.read_csv(csv_file_path_lunar_irradiance)

    elev_shackleton = reader_elev[str(coordinates.get_latitude())]
    lunar_irradiance = reader_lunar_irradiance['Lunar_irr']
    for index_elev, elev in enumerate(elev_shackleton):
        if elev > -limit_angle + AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES:
            irradiance_sun_fully_visible.append(lunar_irradiance[index_elev])
            total_irr_fully_visible += lunar_irradiance[index_elev]

    print(f"The total irradiation received when the Sun is fully visible is {total_irr_fully_visible * 10 ** (-3)}. "
          f" The mean value of irradiance when the Sun is fully visible is : {np.mean(irradiance_sun_fully_visible)}")

    return [total_irr_fully_visible * 10 ** (-3), np.mean(irradiance_sun_fully_visible)]


def get_irradiation_sun_partially_visible(coordinates: Coordinate, csv_file_path_elevations: str,
                                          csv_file_path_lunar_irradiance: str) -> [float, float, float, float]:
    """
    Get the irradiation when the Sun is partially visible, at a chose coordinate and for a chosen period. This function
    also computes the mean irradiance during this period.
    :param csv_file_path_elevations: file path of the elevations
    :param csv_file_path_lunar_irradiance: file path of the lunar irradiance
    :param coordinates: coordinates of the studied point
    :return: the irradiation when the Sun is partially visible, at a chose coordinate and for a chosen period. This
    function also computes the mean irradiance during this period.
    """
    limit_angle = get_angle_sun_visible_beyond_zero(coordinates)
    total_irr_partially_visible = 0
    irradiance_sun_partially_visible = []

    reader_elev = pd.read_csv(csv_file_path_elevations)

    reader_lunar_irradiance = pd.read_csv(csv_file_path_lunar_irradiance)

    elevations_sun = reader_elev[str(coordinates.get_latitude())]
    lunar_irradiance = reader_lunar_irradiance['Lunar_irr']

    for index_elev, elev in enumerate(elevations_sun):
        if -limit_angle - AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES < elev \
                < -limit_angle + AstronomicalConstants.SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES:
            irradiance_sun_partially_visible.append(elev)
            total_irr_partially_visible += lunar_irradiance[index_elev]

    print(
        f"The total irradiation received when the Sun is partially visible is "
        f"{total_irr_partially_visible * 10 ** (-3)}. The mean value of irradiance when the Sun is partially visible "
        f"is : {np.mean(irradiance_sun_partially_visible)}")

    return [total_irr_partially_visible * 10 ** (-3), np.mean(irradiance_sun_partially_visible)]


def get_abs_filepath_from_module(module, relative_file_path):
    """
    Search a the absolute file of a document from the current module.
    :param module: the current module
    :param relative_file_path: file path of the file desired
    :return: the absolute path of the file desired
    """
    return os.path.abspath(os.path.join(os.path.dirname(module), relative_file_path))
