from datetime import datetime

import numpy as np
import pandas as pd
from astropy.time import Time


def get_dynamical_time_from_datetime(timestamp: datetime) -> float:
    """
    Calculates the number of centuries and fraction of centuries between January 1, 2000 at midnight and the timestamp
    (here we call this period Dynamical Time) from Terrestrial time
    :param timestamp: timestamp in format datetime(year, month, date, hours, minutes, seconds)
    :return: Dynamical Time in centuries
    """
    time = Time(timestamp)
    julian_ephemeris_date = time.jd  # Number of days between 'moment' and the beginning of the Julian Period
    # Julian period started at noon on January 1, 4713 B.C.E.

    dynamical_time = (julian_ephemeris_date - 2451545) / 36525
    # 2451545 is the julian date of January 1, 2000 and 36525 is the number of days in a century

    return dynamical_time


def get_dynamical_time_period_array(begin: datetime, end: datetime, frequency: str):
    """
    Get an array of the time period between the beginning and the end with a chosen time frequency
    :param begin: the beginning of the period
    :param end: the end of the period
    :param frequency: the sampling frequency of the period
    :return:
    """
    time_period_cut = pd.date_range(begin, end, freq=frequency)
    get_time = np.vectorize(get_dynamical_time_from_datetime)
    dynamical_time_period_array = get_time(time_period_cut)
    return dynamical_time_period_array
