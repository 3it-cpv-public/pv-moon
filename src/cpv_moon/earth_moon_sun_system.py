from datetime import datetime

import numpy as np

from cpv_moon.arguments_multiplication import ArgumentsMultiplication
from cpv_moon.celest_objects.astronomical_constants import AstronomicalConstants
from cpv_moon.celest_objects.earth import Earth
from cpv_moon.celest_objects.moon import Moon
from cpv_moon.celest_objects.sun import Sun
from cpv_moon.dynamical_time import get_dynamical_time_period_array


class EarthMoonSunSystem:
    """ Define et compute inter-dependant characteristics of the system made by the Earth, the Moon and the Sun
     depending on the dynamical time of the system. Each instance is created for a fixed period of time with an interval
      equals to one hour. """

    def __init__(self, begin: datetime, end: datetime, frequency: str):
        """
        Initialisation of the system for a fixed period of time.
        :param begin: the beginning of the period to define the system and its characteristics
        :param end: the end of the period to define the system and its characteristics
        :param frequency: the frequency of the computation in str format that is compatible with pandas.date_range
        function
        """
        self.earth = Earth()
        self.moon = Moon()
        self.sun = Sun()

        dynamical_time = get_dynamical_time_period_array(begin, end, frequency)
        # the dynamical_time argument allows not to compute the dynamical time every time we call a function
        self._dynamical_time = dynamical_time

    def get_dynamical_time(self) -> np.ndarray:
        return self._dynamical_time

    def get_sun_earth_distance(self) -> float:
        """
        Get the Sun-Earth distance in AU. From MEEUS, chapter 24 (1991)
        :return: the Sun-Earth distance in AU
        """
        # Definition of the dynamical_time
        dynamical_time = self.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        earth_eccentricity = self.earth.get_eccentricity(dynamical_time)
        sun_true_anomaly = self.sun.get_true_anomaly(dynamical_time)

        # Sun radius vector which is the Earth-Sun distance in AU
        return (1.000001018 * (1 - earth_eccentricity ** 2)) / (
                1 + earth_eccentricity * np.cos(np.pi / 180 * sun_true_anomaly))

    def get_earth_moon_distance(self) -> float:
        """
        Get the distance between the centers of Earth and Moon in kilometers. From MEEUS, chapter 45 (1991)
        :return: the distance between the centers of Earth and Moon in kilometers
        """
        # Definition of the dynamical_time
        dynamical_time = self.get_dynamical_time()

        moon_sun_argument_matrix = np.zeros([len(dynamical_time), 4])
        moon_sun_argument_matrix[:, 0] = self.moon.get_elongation(dynamical_time)
        moon_sun_argument_matrix[:, 1] = self.sun.get_mean_anomaly(dynamical_time)
        moon_sun_argument_matrix[:, 2] = self.moon.get_mean_anomaly(dynamical_time)
        moon_sun_argument_matrix[:, 3] = self.moon.get_argument_latitude(dynamical_time)

        earth_decreasing_eccentricity = self.earth.get_decreasing_eccentricity_earth(dynamical_time)

        # Creation of the coefficients used to compute the cosine of the sigma_r term
        coefficients_for_cosine = np.dot(moon_sun_argument_matrix, ArgumentsMultiplication.ARGUMENTS_LONGITUDE_DISTANCE)

        # Coefficient of the cosine of the argument for the distance
        periodic_terms_for_distance_earth_moon = np.array([- 20905355, - 3699111, - 2955968, - 569925, 48888, - 3149,
                                                           246158, - 152138, - 170733, - 204586, - 129620, 108743,
                                                           104755, 10321, 0, 79661, - 34782, - 23210, - 21636, 24208,
                                                           30824, - 8379, - 16675, - 12831, - 10445, - 11650, 14403,
                                                           - 7003, 0, 10056, 6322, - 9884, 5751, 0, - 4950, 4130, 0,
                                                           - 3958, 0, 3258, 2616, - 1897, - 2117, 2354, 0, 0, - 1423,
                                                           - 1117, - 1571, - 1739, 0, - 4421, 0, 0, 0, 0, 1165, 0, 0,
                                                           8752])

        periodic_terms_with_correction = self.correct_sum_coefficient(earth_decreasing_eccentricity,
                                                                      periodic_terms_for_distance_earth_moon,
                                                                   ArgumentsMultiplication.ARGUMENTS_LONGITUDE_DISTANCE)

        cosine_argument_dot_periodic_terms = np.diag(np.dot(np.cos(np.pi / 180 * coefficients_for_cosine),
                                                            periodic_terms_with_correction))

        return 385000.56 + cosine_argument_dot_periodic_terms / 1000

    def get_geocentric_longitude_center_moon(self) -> float:
        """
        Get the geocentric longitude of the Moon's center in degrees. From MEEUS, chapter 45 (1991)
        :return: the geocentric longitude of the Moon's center in degrees
        """
        # Definition of the dynamical_time
        dynamical_time = self.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        moon_argument_latitude = self.moon.get_argument_latitude(dynamical_time)
        moon_longitude = self.moon.get_mean_longitude(dynamical_time)

        moon_sun_argument_matrix = np.zeros([len(dynamical_time), 4])
        moon_sun_argument_matrix[:, 0] = self.moon.get_elongation(dynamical_time)
        moon_sun_argument_matrix[:, 1] = self.sun.get_mean_anomaly(dynamical_time)
        moon_sun_argument_matrix[:, 2] = self.moon.get_mean_anomaly(dynamical_time)
        moon_sun_argument_matrix[:, 3] = self.moon.get_argument_latitude(dynamical_time)

        earth_decreasing_eccentricity = self.earth.get_decreasing_eccentricity_earth(dynamical_time)

        # Argument due to the action of Venus in degrees
        venus_action = 119.75 + 131.849 * dynamical_time
        # Argument due to the action of Jupiter in degrees
        jupiter_action = 53.09 + 479264.29 * dynamical_time

        # Creation of the coefficients used to compute the sine of the sigma_l term
        coefficients_for_sine = np.dot(moon_sun_argument_matrix, ArgumentsMultiplication.ARGUMENTS_LONGITUDE_DISTANCE)

        # Coefficient of the sine of the argument for the longitude
        periodic_terms_for_longitude_moon = np.array([6288774, 1274027, 658314, 213618, - 185116, - 114332, 58793,
                                                      57066, 53322, 45758, - 40923, - 34720, - 30383, 15327, - 12528,
                                                      10980, 10675, 10034, 8548, - 7888, - 6766, - 5163, 4987, 4036,
                                                      3994, 3861, 3665, - 2689, - 2602, 2390, - 2348, 2236, - 2120,
                                                      - 2069, 2048, - 1773, - 1595, 1215, - 1110, - 892, - 810, 759,
                                                      - 713, - 700, 691, 596, 549, 537, 520, - 487, - 399, - 381, 351,
                                                      - 340, 330, 327, - 323, 299, 294, 0])
        periodic_terms_with_correction = self.correct_sum_coefficient(earth_decreasing_eccentricity,
                                                                      periodic_terms_for_longitude_moon,
                                                                   ArgumentsMultiplication.ARGUMENTS_LONGITUDE_DISTANCE)

        sine_argument_dot_periodic_terms = np.diag(np.dot(np.sin(np.pi / 180 * coefficients_for_sine),
                                                          periodic_terms_with_correction))
        corrected_sine_argument_dot_periodic_terms = sine_argument_dot_periodic_terms \
                                                     + 3958 * np.sin(np.pi / 180 * venus_action) \
                                                     + 1962 * np.sin(np.pi / 180 *
                                                                     (moon_longitude - moon_argument_latitude)) \
                                                     + 318 * np.sin(np.pi / 180 * jupiter_action)

        # Geocentric longitude of the center of the Moon in degrees
        geocentric_longitudes_center_moon = moon_longitude + corrected_sine_argument_dot_periodic_terms / 1000000

        return ((geocentric_longitudes_center_moon - 180) % 360) - 180

    def get_geocentric_latitude_center_moon(self) -> float:
        """
        Get the geocentric latitude of the Moon's center in degrees. From MEEUS, chapter 45 (1991)
        :return: the geocentric latitude of the Moon's center in degrees
        """
        # Definition of the dynamical_time
        dynamical_time = self.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        moon_anomaly = self.moon.get_mean_anomaly(dynamical_time)
        moon_argument_latitude = self.moon.get_argument_latitude(dynamical_time)
        moon_longitude = self.moon.get_mean_longitude(dynamical_time)

        moon_sun_argument_matrix = np.zeros([len(moon_argument_latitude), 4])
        moon_sun_argument_matrix[:, 0] = self.moon.get_elongation(dynamical_time)
        moon_sun_argument_matrix[:, 1] = self.sun.get_mean_anomaly(dynamical_time)
        moon_sun_argument_matrix[:, 2] = moon_anomaly
        moon_sun_argument_matrix[:, 3] = moon_argument_latitude

        earth_decreasing_eccentricity = self.earth.get_decreasing_eccentricity_earth(dynamical_time)

        # Argument due to the action of Venus in degrees
        venus_action = 119.75 + 131.849 * dynamical_time
        # Argument in degrees (not precised in Meeus)
        argument_correction = 313.45 + 481266.484 * dynamical_time

        coefficients_for_sine = np.dot(moon_sun_argument_matrix, ArgumentsMultiplication.ARGUMENTS_LATITUDE)

        # Coefficient of the sine of the argument
        periodic_terms_for_latitude_moon = np.array([5128122, 280602, 277693, 173237, 55413, 46217, 32573, 17198,
                                                     9266, 8822, 8216, 4324, 4200, - 3359, 2463, 2211, 2065, - 1870,
                                                     1828, - 1794, - 1749, - 1595, - 1491, - 1475, - 1410, - 1344,
                                                     - 1335, 1107, 1021, 833, 777, 671, 607, 596, 491, - 451, 439, 422,
                                                     421, - 366, - 351, 331, 315, 302, - 283, - 229, 223, 223, - 220,
                                                     - 220, - 185, 181, - 177, 176, 166, - 164, 132, - 119, 115, 107])
        periodic_terms_with_correction = self.correct_sum_coefficient(earth_decreasing_eccentricity,
                                                                      periodic_terms_for_latitude_moon,
                                                                      ArgumentsMultiplication.ARGUMENTS_LATITUDE)

        sine_argument_dot_periodic_terms = np.diag(np.dot(np.sin(np.pi / 180 * coefficients_for_sine),
                                                          periodic_terms_with_correction))
        sine_argument_dot_periodic_terms_real = sine_argument_dot_periodic_terms \
                                                - 2235 * np.sin(np.pi / 180 * moon_longitude) \
                                                + 382 * np.sin(np.pi / 180 * argument_correction) \
                                                + 175 * np.sin(np.pi / 180 * (venus_action - moon_argument_latitude)) \
                                                + 175 * np.sin(np.pi / 180 * (venus_action + moon_argument_latitude)) \
                                                + 127 * np.sin(np.pi / 180 * (moon_longitude - moon_anomaly)) \
                                                - 115 * np.sin(np.pi / 180 * (moon_longitude + moon_anomaly))

        # Geocentric latitude of the center of the Moon in degrees
        geocentric_latitude_center_moon = sine_argument_dot_periodic_terms_real / 1000000

        return ((geocentric_latitude_center_moon - 90) % 180) - 90

    def get_nutation_earth(self) -> float:
        """
        Calculates the nutation in longitude of the Earth  in degrees. From MEEUS, chapter 21 (1991)
        Nutation subtly changes the axial tilt of Earth with respect to the ecliptic plane, shifting the major circles
        of latitude that are defined by the Earth's tilt. (wikipedia)
        :return: Nutation in longitude of the Earth in degrees
        """
        # Definition of the dynamical_time
        dynamical_time = self.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        moon_elongation = self.moon.get_elongation(dynamical_time)
        moon_anomaly = self.moon.get_mean_anomaly(dynamical_time)
        moon_argument_latitude = self.moon.get_argument_latitude(dynamical_time)
        moon_omega = self.moon.get_omega(dynamical_time)
        sun_anomaly = self.sun.get_mean_anomaly(dynamical_time)

        moon_sun_argument_matrix = np.zeros((len(moon_argument_latitude), 5))

        moon_sun_argument_matrix[:, 0] = moon_elongation
        moon_sun_argument_matrix[:, 1] = sun_anomaly
        moon_sun_argument_matrix[:, 2] = moon_anomaly
        moon_sun_argument_matrix[:, 3] = moon_argument_latitude
        moon_sun_argument_matrix[:, 4] = moon_omega

        #                               Periodic terms for the nutation in longitude

        # argument multiple of moon_elongation, sun_anomaly, moon_anomaly, moon_argument_latitude and moon_omega
        argument_multiple_useful_terms = np.transpose(
            np.array([[0, 0, 0, 0, 1], [-2, 0, 0, 2, 2], [0, 0, 0, 2, 2], [0, 0, 0, 0, 2], [0, 1, 0, 0, 0],
                      [0, 0, 1, 0, 0], [-2, 1, 0, 2, 2], [0, 0, 0, 2, 1], [0, 0, 1, 2, 2], [-2, -1, 0, 2, 2],
                      [- 2, 0, 1, 0, 0], [-2, 0, 0, 2, 1], [0, 0, -1, 2, 2], [2, 0, 0, 0, 0], [0, 0, 1, 0, 1],
                      [2, 0, -1, 2, 2], [0, 0, -1, 0, 1], [0, 0, 1, 2, 1], [-2, 0, 2, 0, 0], [0, 0, -2, 2, 1],
                      [2, 0, 0, 2, 2], [0, 0, 2, 2, 2], [0, 0, 2, 0, 0], [-2, 0, 1, 2, 2], [0, 0, 0, 2, 0],
                      [-2, 0, 0, 2, 0], [0, 0, -1, 2, 1], [0, 2, 0, 0, 0], [2, 0, -1, 0, 1], [-2, 2, 0, 2, 2],
                      [0, 1, 0, 0, 1], [-2, 0, 1, 0, 1], [0, -1, 0, 0, 1], [0, 0, 2, -2, 0], [2, 0, -1, 2, 1],
                      [2, 0, 1, 2, 2], [0, 1, 0, 2, 2], [-2, 1, 1, 0, 0], [0, -1, 0, 2, 2], [2, 0, 0, 2, 1],
                      [2, 0, 1, 0, 0], [-2, 0, 2, 2, 2], [-2, 0, 1, 2, 1], [2, 0, -2, 0, 1], [2, 0, 0, 0, 1],
                      [0, -1, 1, 0, 0], [-2, -1, 0, 2, 1], [-2, 0, 0, 0, 1], [0, 0, 2, 2, 1], [-2, 0, 2, 0, 1],
                      [-2, 1, 0, 2, 1], [0, 0, 1, -2, 0], [-1, 0, 1, 0, 0], [-2, 1, 0, 0, 0], [1, 0, 0, 0, 0],
                      [0, 0, 1, 2, 0], [0, 0, -2, 2, 2], [-1, -1, 1, 0, 0], [0, 1, 1, 0, 0], [0, -1, 1, 2, 2],
                      [2, -1, -1, 2, 2], [0, 0, 3, 2, 2], [2, -1, 0, 2, 2]]))

        coefficients_for_sine = np.dot(moon_sun_argument_matrix, argument_multiple_useful_terms)

        #                           Computation of the nutation in longitude in degrees
        nutation_earth = np.zeros([len(dynamical_time), 1])

        for instant_t, dynamical_time_t in enumerate(dynamical_time):
            # Coefficient of the sine of the argument
            periodic_terms_for_nutation_longitude = np.array(
                [-171996 - 174.2 * dynamical_time_t, -13187 - 1.6 * dynamical_time_t,
                 -2274 - 0.2 * dynamical_time_t, 2062 + 0.2 * dynamical_time_t,
                 1426 - 3.4 * dynamical_time_t, 712 + 0.1 * dynamical_time_t,
                 -517 + 1.2 * dynamical_time_t, -386 - 0.4 * dynamical_time_t, - 301,
                 217 - 0.5 * dynamical_time_t, - 158, 129 + 0.1 * dynamical_time_t, 123,
                 63, 63 + 0.1 * dynamical_time_t, -59, -58 - 0.1 * dynamical_time_t, -51,
                 48, 46, -38, -31, 29, 29, 26, - 22, 21, 17 - 0.1 * dynamical_time_t, 16,
                 -16 + 0.1 * dynamical_time_t, -15, -13, -12, 11, -10, -8, 7, -7, -7, -7,
                 6, 6, 6, - 6, -6, 5, -5, -5, -5, 4, 4, 4, -4, -4, -4, 3, -3, -3, -3, -3,
                 -3, -3, -3])

            nutation_earth[instant_t] = np.dot(np.sin(np.pi / 180 * coefficients_for_sine[instant_t, :]),
                                               periodic_terms_for_nutation_longitude) / 36000000

        return ((nutation_earth.ravel() - 180) % 360) - 180

    def get_heliocentric_ecliptical_longitude_moon(self) -> float:
        """
        Get the ecliptical longitude of the Moon in degrees in the Sun's coordinate system.
        From MEEUS, chapter 51 (1991)
        :return: the ecliptical longitude of the Moon in degrees in the Sun's coordinate system
        """
        # Definition of the dynamical_time
        dynamical_time = self.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        apparent_geocentric_longitude_sun = self.sun.get_apparent_longitude_sun(dynamical_time)
        distance_earth_sun = self.get_sun_earth_distance()
        distance_earth_sun_in_km = distance_earth_sun * AstronomicalConstants.AU_IN_KM
        distance_earth_moon = self.get_earth_moon_distance()
        geocentric_longitude_center_moon = self.get_geocentric_longitude_center_moon()
        geocentric_latitude_center_moon = self.get_geocentric_latitude_center_moon()

        # Computation of the longitude and latitude of the Moon in the Sun's coordinate system
        heliocentric_ecliptical_longitude_moon = apparent_geocentric_longitude_sun + 180 + (
                distance_earth_moon / distance_earth_sun_in_km) * 57.296 * np.cos(
            np.pi / 180 * geocentric_latitude_center_moon) * np.sin(
            np.pi / 180 * (apparent_geocentric_longitude_sun - geocentric_longitude_center_moon))

        return heliocentric_ecliptical_longitude_moon

    def get_heliocentric_ecliptical_lat_moon(self) -> float:
        """
        Get the ecliptical latitude of the Moon in degrees in the Sun's coordinate system. From MEEUS 1991 (chapter 51)
        :return: the ecliptical latitude of the Moon in degrees in the Sun's coordinate system
        """
        # Creation of the useful terms from the celestial objects
        distance_earth_sun = self.get_sun_earth_distance()
        distance_earth_sun_in_km = distance_earth_sun * AstronomicalConstants.AU_IN_KM
        distance_earth_moon = self.get_earth_moon_distance()
        geocentric_latitude_center_moon = self.get_geocentric_latitude_center_moon()

        # Computation of the longitude and latitude of the Moon in the Sun's coordinate system
        heliocentric_ecliptical_latitude_moon = (distance_earth_moon / distance_earth_sun_in_km) \
                                                * geocentric_latitude_center_moon

        return heliocentric_ecliptical_latitude_moon

    @staticmethod
    def correct_sum_coefficient(earth_decreasing_eccentricity: np.ndarray, sum_coefficient: np.ndarray,
                                arguments_sum: ArgumentsMultiplication) -> np.ndarray:
        """
        Correct the sum of the periodic terms for the distance Earth-Moon and the longitude and latitude of the Moon in
        the geocentric coordinates system. This function implies to take into account the decreasing of the
        eccentricity of the Earth in the periodic terms.
        :param earth_decreasing_eccentricity: coefficients of the decreasing eccentricity
        :param sum_coefficient: sum of the coefficient that we want to correct
        :param arguments_sum: arguments that indicate if a term needs to be corrected or not
        :return: the corrected sum of coefficient with the implication of the decreasing eccentricity of the Earth
        """
        sum_coefficients_corrected = np.zeros([len(sum_coefficient), len(earth_decreasing_eccentricity)])

        for (index_line, index_column), _ in np.ndenumerate(sum_coefficients_corrected):
            if arguments_sum[1, index_line] == 1 or arguments_sum[1, index_line] == - 1:
                sum_coefficients_corrected[index_line, index_column] = sum_coefficient[index_line] * \
                                                                       earth_decreasing_eccentricity[index_column]

            elif arguments_sum[1, index_line] == 2 or arguments_sum[1, index_line] == - 2:
                sum_coefficients_corrected[index_line, index_column] = sum_coefficient[index_line] * \
                                                                       earth_decreasing_eccentricity[index_column] ** 2
            else:
                sum_coefficients_corrected[index_line, index_column] = sum_coefficient[index_line]

        return sum_coefficients_corrected
