from datetime import datetime


class DateBoundaries:
    """
    This class synthesizes the dates used for the moon years
    """
    date_boundaries = {
        "1972": {
            "begin": datetime(year=1972, month=2, day=15, hour=0, minute=0, second=0),
            "end": datetime(year=1973, month=2, day=2, hour=23, minute=0, second=0)
        },
        "1973": {
            "begin": datetime(year=1973, month=2, day=3, hour=0, minute=0, second=0),
            "end": datetime(year=1974, month=1, day=22, hour=23, minute=0, second=0)
        },
        "1974": {
            "begin": datetime(year=1974, month=1, day=23, hour=0, minute=0, second=0),
            "end": datetime(year=1975, month=2, day=10, hour=23, minute=0, second=0)
        },
        "1975": {
            "begin": datetime(year=1975, month=2, day=11, hour=0, minute=0, second=0),
            "end": datetime(year=1976, month=1, day=30, hour=23, minute=0, second=0)
        },
        "1976": {
            "begin": datetime(year=1976, month=1, day=31, hour=0, minute=0, second=0),
            "end": datetime(year=1977, month=2, day=17, hour=23, minute=0, second=0)
        },
        "1977": {
            "begin": datetime(year=1977, month=2, day=18, hour=0, minute=0, second=0),
            "end": datetime(year=1978, month=2, day=6, hour=23, minute=0, second=0)
        },
        "1978": {
            "begin": datetime(year=1978, month=2, day=7, hour=0, minute=0, second=0),
            "end": datetime(year=1979, month=1, day=27, hour=23, minute=0, second=0)
        },
        "1979": {
            "begin": datetime(year=1979, month=1, day=28, hour=0, minute=0, second=0),
            "end": datetime(year=1980, month=2, day=15, hour=23, minute=0, second=0)
        },
        "1980": {
            "begin": datetime(year=1980, month=2, day=16, hour=0, minute=0, second=0),
            "end": datetime(year=1981, month=2, day=4, hour=23, minute=0, second=0)
        },
        "1981": {
            "begin": datetime(year=1981, month=2, day=5, hour=0, minute=0, second=0),
            "end": datetime(year=1982, month=1, day=24, hour=23, minute=0, second=0)
        },
        "1982": {
            "begin": datetime(year=1982, month=1, day=25, hour=0, minute=0, second=0),
            "end": datetime(year=1983, month=2, day=12, hour=23, minute=0, second=0)
        },
        "1983": {
            "begin": datetime(year=1983, month=2, day=13, hour=0, minute=0, second=0),
            "end": datetime(year=1984, month=2, day=1, hour=23, minute=0, second=0)
        },
        "1984": {
            "begin": datetime(year=1984, month=2, day=2, hour=0, minute=0, second=0),
            "end": datetime(year=1985, month=2, day=19, hour=23, minute=0, second=0)
        },
        "1985": {
            "begin": datetime(year=1985, month=2, day=20, hour=0, minute=0, second=0),
            "end": datetime(year=1986, month=2, day=8, hour=23, minute=0, second=0)
        },
        "1986": {
            "begin": datetime(year=1986, month=2, day=9, hour=0, minute=0, second=0),
            "end": datetime(year=1987, month=1, day=28, hour=23, minute=0, second=0)
        },
        "1987": {
            "begin": datetime(year=1987, month=1, day=29, hour=0, minute=0, second=0),
            "end": datetime(year=1988, month=2, day=16, hour=23, minute=0, second=0)
        },
        "1988": {
            "begin": datetime(year=1988, month=2, day=17, hour=0, minute=0, second=0),
            "end": datetime(year=1989, month=2, day=5, hour=23, minute=0, second=0)
        },
        "1989": {
            "begin": datetime(year=1989, month=2, day=6, hour=0, minute=0, second=0),
            "end": datetime(year=1990, month=1, day=26, hour=23, minute=0, second=0)
        },
        "1990": {
            "begin": datetime(year=1990, month=1, day=27, hour=0, minute=0, second=0),
            "end": datetime(year=1991, month=2, day=14, hour=23, minute=0, second=0)
        },
        "1991": {
            "begin": datetime(year=1991, month=2, day=15, hour=0, minute=0, second=0),
            "end": datetime(year=1992, month=2, day=3, hour=23, minute=0, second=0)
        },
        "1992": {
            "begin": datetime(year=1992, month=2, day=4, hour=0, minute=0, second=0),
            "end": datetime(year=1993, month=1, day=22, hour=23, minute=0, second=0)
        },
        "1993": {
            "begin": datetime(year=1993, month=1, day=23, hour=0, minute=0, second=0),
            "end": datetime(year=1994, month=2, day=9, hour=23, minute=0, second=0)
        },
        "1994": {
            "begin": datetime(year=1994, month=2, day=10, hour=0, minute=0, second=0),
            "end": datetime(year=1995, month=1, day=30, hour=23, minute=0, second=0)
        },
        "1995": {
            "begin": datetime(year=1995, month=1, day=31, hour=0, minute=0, second=0),
            "end": datetime(year=1996, month=2, day=18, hour=23, minute=0, second=0)
        },
        "1996": {
            "begin": datetime(year=1996, month=2, day=19, hour=0, minute=0, second=0),
            "end": datetime(year=1997, month=2, day=6, hour=23, minute=0, second=0)
        },
        "1997": {
            "begin": datetime(year=1997, month=2, day=7, hour=0, minute=0, second=0),
            "end": datetime(year=1998, month=1, day=27, hour=23, minute=0, second=0)
        },
        "1998": {
            "begin": datetime(year=1998, month=1, day=28, hour=0, minute=0, second=0),
            "end": datetime(year=1999, month=2, day=15, hour=23, minute=0, second=0)
        },
        "1999": {
            "begin": datetime(year=1999, month=2, day=16, hour=0, minute=0, second=0),
            "end": datetime(year=2000, month=2, day=4, hour=23, minute=0, second=0)
        },
        "2000": {
            "begin": datetime(year=2000, month=2, day=5, hour=0, minute=0, second=0),
            "end": datetime(year=2001, month=1, day=23, hour=23, minute=0, second=0)
        },
        "2001": {
            "begin": datetime(year=2001, month=1, day=24, hour=0, minute=0, second=0),
            "end": datetime(year=2002, month=2, day=11, hour=23, minute=0, second=0)
        },
        "2002": {
            "begin": datetime(year=2002, month=2, day=12, hour=0, minute=0, second=0),
            "end": datetime(year=2003, month=1, day=31, hour=23, minute=0, second=0)
        },
        "2003": {
            "begin": datetime(year=2003, month=2, day=1, hour=0, minute=0, second=0),
            "end": datetime(year=2004, month=1, day=21, hour=23, minute=0, second=0)
        },
        "2004": {
            "begin": datetime(year=2004, month=1, day=22, hour=0, minute=0, second=0),
            "end": datetime(year=2005, month=2, day=8, hour=23, minute=0, second=0)
        },
        "2005": {
            "begin": datetime(year=2005, month=2, day=9, hour=0, minute=0, second=0),
            "end": datetime(year=2006, month=1, day=28, hour=23, minute=0, second=0)
        },
        "2006": {
            "begin": datetime(year=2006, month=1, day=29, hour=0, minute=0, second=0),
            "end": datetime(year=2007, month=2, day=17, hour=23, minute=0, second=0)
        },
        "2007": {
            "begin": datetime(year=2007, month=2, day=18, hour=0, minute=0, second=0),
            "end": datetime(year=2008, month=2, day=6, hour=23, minute=0, second=0)
        },
        "2008": {
            "begin": datetime(year=2008, month=2, day=7, hour=0, minute=0, second=0),
            "end": datetime(year=2009, month=1, day=25, hour=23, minute=0, second=0)
        },
        "2009": {
            "begin": datetime(year=2009, month=1, day=26, hour=0, minute=0, second=0),
            "end": datetime(year=2010, month=2, day=13, hour=23, minute=0, second=0)
        },
        "2010": {
            "begin": datetime(year=2010, month=2, day=14, hour=0, minute=0, second=0),
            "end": datetime(year=2011, month=2, day=2, hour=23, minute=0, second=0)
        },
        "2011": {
            "begin": datetime(year=2011, month=2, day=3, hour=0, minute=0, second=0),
            "end": datetime(year=2012, month=1, day=22, hour=23, minute=0, second=0)
        },
        "2012": {
            "begin": datetime(year=2012, month=1, day=23, hour=0, minute=0, second=0),
            "end": datetime(year=2013, month=2, day=9, hour=23, minute=0, second=0)
        },
        "2013": {
            "begin": datetime(year=2013, month=2, day=10, hour=0, minute=0, second=0),
            "end": datetime(year=2014, month=1, day=30, hour=23, minute=0, second=0)
        },
        "2014": {
            "begin": datetime(year=2014, month=1, day=31, hour=0, minute=0, second=0),
            "end": datetime(year=2015, month=2, day=18, hour=23, minute=0, second=0)
        },
        "2015": {
            "begin": datetime(year=2015, month=2, day=19, hour=0, minute=0, second=0),
            "end": datetime(year=2016, month=2, day=7, hour=23, minute=0, second=0)
        },
        "2016": {
            "begin": datetime(year=2016, month=2, day=8, hour=0, minute=0, second=0),
            "end": datetime(year=2017, month=1, day=27, hour=23, minute=0, second=0),
        },
        "2017": {
            "begin": datetime(year=2017, month=1, day=28, hour=0, minute=0, second=0),
            "end": datetime(year=2018, month=2, day=15, hour=23, minute=0, second=0)
        },
        "2018": {
            "begin": datetime(year=2018, month=2, day=16, hour=0, minute=0, second=0),
            "end": datetime(year=2019, month=2, day=4, hour=23, minute=0, second=0)
        },
        "2019": {
            "begin": datetime(year=2019, month=2, day=5, hour=0, minute=0, second=0),
            "end": datetime(year=2020, month=1, day=24, hour=23, minute=0, second=0)
        },
        "2020": {
            "begin": datetime(year=2020, month=1, day=25, hour=0, minute=0, second=0),
            "end": datetime(year=2021, month=2, day=11, hour=23, minute=0, second=0)
        },
        "2021": {
            "begin": datetime(year=2021, month=2, day=12, hour=0, minute=0, second=0),
            "end": datetime(year=2022, month=1, day=31, hour=23, minute=0, second=0)
        },
        "2022": {
            "begin": datetime(year=2022, month=2, day=1, hour=0, minute=0, second=0),
            "end": datetime(year=2023, month=1, day=21, hour=23, minute=0, second=0)
        },
        "2023": {
            "begin": datetime(year=2023, month=1, day=22, hour=0, minute=0, second=0),
            "end": datetime(year=2024, month=2, day=9, hour=23, minute=0, second=0)
        },
        "2024": {
            "begin": datetime(year=2024, month=2, day=10, hour=0, minute=0, second=0),
            "end": datetime(year=2025, month=1, day=28, hour=23, minute=0, second=0)
        },
        "2025": {
            "begin": datetime(year=2025, month=1, day=29, hour=0, minute=0, second=0),
            "end": datetime(year=2026, month=2, day=16, hour=23, minute=0, second=0)
        },
        "2026": {
            "begin": datetime(year=2026, month=2, day=17, hour=0, minute=0, second=0),
            "end": datetime(year=2027, month=2, day=5, hour=23, minute=0, second=0)
        },
        "2027": {
            "begin": datetime(year=2027, month=2, day=6, hour=0, minute=0, second=0),
            "end": datetime(year=2028, month=1, day=25, hour=23, minute=0, second=0)
        },
        "2028": {
            "begin": datetime(year=2028, month=1, day=26, hour=0, minute=0, second=0),
            "end": datetime(year=2029, month=2, day=12, hour=23, minute=0, second=0)
        },
        "2029": {
            "begin": datetime(year=2029, month=2, day=13, hour=0, minute=0, second=0),
            "end": datetime(year=2030, month=2, day=2, hour=23, minute=0, second=0)
        },
        "2030": {
            "begin": datetime(year=2030, month=2, day=3, hour=0, minute=0, second=0),
            "end": datetime(year=2031, month=1, day=22, hour=23, minute=0, second=0)
        },
        "2031": {
            "begin": datetime(year=2031, month=1, day=23, hour=0, minute=0, second=0),
            "end": datetime(year=2032, month=2, day=10, hour=23, minute=0, second=0)
        },
        "2032": {
            "begin": datetime(year=2032, month=2, day=11, hour=0, minute=0, second=0),
            "end": datetime(year=2033, month=1, day=30, hour=23, minute=0, second=0)
        }
    }
