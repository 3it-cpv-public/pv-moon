class AstronomicalConstants:
    """ Some astronomical constants useful for our main code"""

    # Astronomical unit in km
    AU_IN_KM = 149597870.7

    # Solar Constant in W / m ^ 2
    SOLAR_CONSTANT_IN_W_M2 = 1361.8

    # Semi apparent diameter of the Sun seeing from the Moon
    SEMI_APPARENT_DIAMETER_SUN_IN_DEGREES = 0.265

    # For latitudes superior than 70°, we consider the Sun with a apparent diameter of 0.53° because the elevation
    # varies around zero with low amplitude. For the rest, the Sun is considered as a dot.
    LIMIT_LATITUDE_APPARENT_DIAMETER_SUN = 70

    # Moon radius in km
    MOON_RADIUS_IN_KM = 1737.1
