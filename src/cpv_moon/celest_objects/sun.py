from abc import ABC

import numpy as np

from cpv_moon.celest_objects.celest_object import CelestObject


class Sun(CelestObject, ABC):
    """ Define celestial mechanical characteristics of the Sun for a chosen moment """

    def get_mean_longitude(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        # From MEEUS, chapter 24 (1991)
        return 280.46645 + 36000.76983 * dynamical_time_in_centuries \
               + 0.0003032 * (dynamical_time_in_centuries ** 2)

    def get_mean_anomaly(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        # From MEEUS, chapter 45 (1991)
        return 357.52910921 + 35999.0502909 * dynamical_time_in_centuries - 0.0001536 * (
                dynamical_time_in_centuries ** 2) + (dynamical_time_in_centuries ** 3) / 24490000

    def get_equation_of_center_sun(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the sun's equation of center in degrees. From MEEUS, chapter 24 (1991)
        The equation of the center is the difference between the true anomaly and the mean anomaly. (wikipedia)
        :param dynamical_time_in_centuries: dynamical time in centuries, computed thanks to get_dynamical_time function
        :return: the sun's equation of center in degrees
        """
        sun_anomaly = self.get_mean_anomaly(dynamical_time_in_centuries)

        return (1.9146 - 0.004817 * dynamical_time_in_centuries - 1.4e-05 * (
                dynamical_time_in_centuries ** 2)) * np.sin(np.pi / 180 * sun_anomaly) \
               + (0.019993 - 0.000101 * dynamical_time_in_centuries) * np.sin(np.pi / 180 * 2 * sun_anomaly) \
               + 0.00029 * np.sin(np.pi / 180 * 3 * sun_anomaly)

    def get_omega(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        # Precision : get the term for correction to obtain apparent longitude of the Sun.
        # From MEEUS, chapter 24 (1991)
        return 125.04 - 1934.136 * dynamical_time_in_centuries

    def get_true_anomaly(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the Sun's true anomaly in degrees. From MEEUS, chapter 24 (1991)
        It is the angle between the direction of periapsis and the current position of the Sun, as seen from the main
        focus of the ellipse (the point around which the Sun orbits). (wikipedia)
        :param dynamical_time_in_centuries: dynamical time in centuries, computed thanks to get_dynamical_time function
        :return: the Sun's true anomaly in degrees
        """
        sun_anomaly = self.get_mean_anomaly(dynamical_time_in_centuries)
        sun_equation_of_center = self.get_equation_of_center_sun(dynamical_time_in_centuries)

        return sun_equation_of_center + sun_anomaly

    def get_true_longitude(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the Sun's true longitude in degrees. From MEEUS, chapter 24 (1991)
        True longitude of the Sun is the ecliptic longitude at which the Sun could actually be found if its inclination
        were zero. (wikipedia)
        :param dynamical_time_in_centuries: dynamical time in centuries, computed thanks to get_dynamical_time function
        :return: the Sun's true longitude in degrees
        """
        sun_mean_longitude = self.get_mean_longitude(dynamical_time_in_centuries)
        sun_center = self.get_equation_of_center_sun(dynamical_time_in_centuries)

        true_longitude_sun = sun_mean_longitude + sun_center

        return true_longitude_sun % 360

    def get_apparent_longitude_sun(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the apparent Sun's longitude in degrees. From MEEUS, chapter 24 (1991)
        Apparent longitude is celestial longitude corrected for aberration and nutation as opposed to mean longitude.
        (wikipedia)
        :return: the apparent Sun's longitude in degrees
        """
        sun_omega = self.get_omega(dynamical_time_in_centuries)

        # Getting the true longitude of the sun from the get_true_longitude_sun self method
        true_longitude_sun = self.get_true_longitude(dynamical_time_in_centuries)

        # Apparent longitude of the Sun referred to the true equinox of the date
        apparent_sun_longitude = true_longitude_sun - 0.00569 - 0.00478 * np.sin(np.pi / 180 * sun_omega)

        return apparent_sun_longitude

    def get_eccentricity(self, dynamical_time_in_centuries: np.ndarray):
        assert False, "This is not a Sun's property useful for the final computation"

    def get_elongation(self, dynamical_time_in_centuries: np.ndarray):
        assert False, "This is not a Sun's property useful for the final computation"

    def get_inclination(self):
        assert False, "This is not a Sun's property useful for the final computation"

    def get_radius(self):
        assert False, "This is not a Sun's property useful for the final computation"
