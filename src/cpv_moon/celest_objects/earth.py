from abc import ABC

import numpy as np

from cpv_moon.celest_objects.celest_object import CelestObject


class Earth(CelestObject, ABC):
    """ Define celestial mechanical characteristics of the Earth for a chosen moment """

    def get_eccentricity(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        # From MEEUS, chapter 24 (1991)
        return 0.016708617 - 4.2037e-05 * dynamical_time_in_centuries - 1.236e-07 * (
                dynamical_time_in_centuries ** 2)

    @staticmethod
    def get_decreasing_eccentricity_earth(dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the term that takes into account that Earth's eccentricity is decreasing over time.
        From MEEUS, chapter 45 (1991)
        :param dynamical_time_in_centuries: dynamical time in centuries, computed thanks to get_dynamical_time function
        :return: the term that takes into account that Earth's eccentricity is decreasing over time
        """
        return 1 - 0.002516 * dynamical_time_in_centuries - 7.4e-06 * dynamical_time_in_centuries ** 2

    def get_mean_longitude(self, dynamical_time_in_centuries: np.ndarray):
        assert False, "This is not an Earth's property useful for the final computation"

    def get_mean_anomaly(self, dynamical_time_in_centuries: np.ndarray):
        assert False, "This is not an Earth's property useful for the final computation"

    def get_elongation(self, dynamical_time_in_centuries: np.ndarray):
        assert False, "This is not an Earth's property useful for the final computation"

    def get_omega(self, dynamical_time_in_centuries: np.ndarray):
        assert False, "This is not an Earth's property useful for the final computation"

    def get_radius(self):
        assert False, "This is not an Earth's property useful for the final computation"

    def get_inclination(self):
        assert False, "This is not an Earth's property useful for the final computation"
