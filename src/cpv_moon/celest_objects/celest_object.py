from abc import ABC, abstractmethod

import numpy as np


class CelestObject(ABC):
    """ Gives information about the celestial position and characteristics of a celestial object """

    @abstractmethod
    def get_mean_longitude(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the mean longitude of the celestial object in degrees and referred to the mean equinox of the date.
        The mean longitude is the ecliptic longitude at which an orbiting body could be found if its orbit were circular
        and free of perturbations. (wikipedia)
        :param dynamical_time_in_centuries: array of dynamical time in centuries, computed thanks to
        get_dynamical_time_period_array function
        :return: the mean longitude of the object in degrees
        """

    @abstractmethod
    def get_mean_anomaly(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the mean anomaly of the celestial object in degrees.
        It is the fraction of an elliptical orbit's period that has elapsed since the orbiting body passed periapsis,
        expressed as an angle which can be used in calculating the position of that body in the classical two-body
        problem. (wikipedia)
        :param dynamical_time_in_centuries: array of dynamical time in centuries, computed thanks to
        get_dynamical_time_period_array function
        :return: the mean anomaly of the object in degrees
        """

    @abstractmethod
    def get_elongation(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the elongation of the celestial object in degrees.
        A body's elongation is the angular separation between the Sun and the planet, with Earth as the reference
        point. (wikipedia)
        :param dynamical_time_in_centuries: array of dynamical time in centuries, computed thanks to
        get_dynamical_time_period_array function
        :return: the elongation of the object in degrees
        """

    @abstractmethod
    def get_omega(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the parameter omega of the celestial object in a way defined according to the object. The definition of
        omega depends on the object.
        :param dynamical_time_in_centuries: array of dynamical time in centuries, computed thanks to
        get_dynamical_time_period_array function
        :return: the parameter omega of the object in degrees
        """

    @abstractmethod
    def get_eccentricity(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the eccentricity of the celestial object.
        The orbital eccentricity of an astronomical object is a dimensionless parameter that determines the amount by
        which its orbit around another body deviates from a perfect circle. (wikipedia)
        :param dynamical_time_in_centuries: array of dynamical time in centuries, computed thanks to
        get_dynamical_time_period_array function
        :return: the eccentricity of the celestial object without dimension
        """

    @abstractmethod
    def get_radius(self) -> float:
        """
        Get the radius of the celestial object in astronomical units.
        :return: the radius of the celestial object in astronomical units
        """

    @abstractmethod
    def get_inclination(self) -> float:
        """
        Get the inclination of the celestial object in degrees. In the solar system, the inclination is the angle
        between the orbital and the ecliptical plan.
        :return: the inclination of the celestial object in degrees
        """
