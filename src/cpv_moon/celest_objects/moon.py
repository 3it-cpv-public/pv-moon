from abc import ABC

import numpy as np

from cpv_moon.celest_objects.celest_object import CelestObject


class Moon(CelestObject, ABC):
    """ Define celestial mechanical characteristics of the Moon for a chosen moment """

    def get_elongation(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        # From MEEUS, chapter 45 (1991)
        return 297.8502042 + 445267.1115168 * dynamical_time_in_centuries \
               - 0.00163 * (dynamical_time_in_centuries ** 2) \
               + (dynamical_time_in_centuries ** 3) / 545868 \
               - (dynamical_time_in_centuries ** 4) / 113065000

    def get_mean_longitude(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        # Precision : the computation includes the constant term of the effect of light-time.
        # From MEEUS, chapter 45 (1991)
        return 218.3164591 + 481267.88134236 * dynamical_time_in_centuries - 0.0013268 * (
                dynamical_time_in_centuries ** 2) + (dynamical_time_in_centuries ** 3) / 538841 \
               - (dynamical_time_in_centuries ** 4) / 65194000

    def get_mean_anomaly(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        # From MEEUS, chapter 45 (1991)
        return 134.9634114 + 477198.8676313 * dynamical_time_in_centuries + 0.008997 * (
                dynamical_time_in_centuries ** 2) + (dynamical_time_in_centuries ** 3) / 69699 - (
                       dynamical_time_in_centuries ** 4) / 14712000

    def get_omega(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        # Precision : get the longitude in degrees of the ascending node of the Moon's mean orbit on the ecliptic
        # measured from the mean equinox of the date. From MEEUS, chapter 45 (1991)
        omega = 125.044555 - 1934.1361849 * dynamical_time_in_centuries + 0.0020762 * (
                dynamical_time_in_centuries ** 2) + (dynamical_time_in_centuries ** 3) / 467410 - (
                        dynamical_time_in_centuries ** 4) / 60616000

        return omega % 360

    @staticmethod
    def get_argument_latitude(dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the Moon's argument of latitude in degrees.  From MEEUS, chapter 45 (1991)
        It is the angle between the ascending node of the Moon and the Moon. (wikipedia)
        :param dynamical_time_in_centuries: dynamical time in centuries, computed thanks to get_dynamical_time function
        :return: the Moon's argument of latitude in degrees
        """
        return 93.2720993 + 483202.0175273 * dynamical_time_in_centuries - 0.0034029 * (
                dynamical_time_in_centuries ** 2) - (dynamical_time_in_centuries ** 3) / 3526000 \
               + (dynamical_time_in_centuries ** 4) / 863310000

    def get_radius(self) -> float:
        return 1.161363636e-5

    def get_inclination(self) -> float:
        return 1.54242

    def get_physical_libration_inclination(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the Moon's physical libration in degrees in the inclination of the lunar equator to the ecliptic.
         From ECKHARDT (1981) and MEEUS, chapter 51 (1991)
        Physical libration is the oscillation of orientation in space about uniform rotation and precession. (wikipedia)
        :param dynamical_time_in_centuries: dynamical time in centuries, computed thanks to get_dynamical_time function
        :return: the Moon's physical libration in degrees in inclination
        """
        # Creation of the useful terms
        anomaly_moon = self.get_mean_anomaly(dynamical_time_in_centuries)
        argument_latitude_moon = self.get_argument_latitude(dynamical_time_in_centuries)
        elongation_moon = self.get_elongation(dynamical_time_in_centuries)

        physical_libration_inclination = - 0.02752 * np.cos(np.pi / 180 * anomaly_moon) - 0.02245 * np.sin(
            np.pi / 180 * argument_latitude_moon) \
                                         + 0.00684 * np.cos(
            np.pi / 180 * (anomaly_moon - 2 * argument_latitude_moon)) - 0.00293 * np.cos(
            np.pi / 180 * 2 * argument_latitude_moon) \
                                         - 0.00085 * np.cos(
            np.pi / 180 * (2 * argument_latitude_moon - 2 * elongation_moon)) - 0.00054 * np.cos(
            np.pi / 180 * (anomaly_moon - 2 * elongation_moon)) \
                                         - 0.0002 * np.sin(
            np.pi / 180 * (anomaly_moon + argument_latitude_moon)) - 0.0002 * np.cos(
            np.pi / 180 * (anomaly_moon + 2 * argument_latitude_moon)) \
                                         - 0.0002 * np.cos(np.pi / 180 * (anomaly_moon - argument_latitude_moon)) \
                                         + 0.00014 * np.cos(
            np.pi / 180 * (anomaly_moon + 2 * argument_latitude_moon - 2 * elongation_moon))

        return physical_libration_inclination

    def get_physical_libration_node(self, dynamical_time_in_centuries: np.ndarray) -> np.ndarray:
        """
        Get the Moon's physical libration in degrees in the longitude of the descending node of the lunar equator
         From ECKHARDT (1981) and MEEUS, chapter 51 (1991)
        Physical libration is the oscillation of orientation in space about uniform rotation and precession. (wikipedia)
        :param dynamical_time_in_centuries: dynamical time in centuries, computed thanks to get_dynamical_time function
        :return: the Moon's physical libration in degrees in the longitude of the descending node of the lunar equator
        """
        # Creation of the useful terms
        anomaly_moon = self.get_mean_anomaly(dynamical_time_in_centuries)
        argument_latitude_moon = self.get_argument_latitude(dynamical_time_in_centuries)
        elongation_moon = self.get_elongation(dynamical_time_in_centuries)

        physical_libration_node = - 0.02816 * np.sin(np.pi / 180 * anomaly_moon) \
                                  + 0.02244 * np.cos(np.pi / 180 * argument_latitude_moon) \
                                  - 0.00682 * np.sin(
            np.pi / 180 * (anomaly_moon - 2 * argument_latitude_moon)) - 0.00279 * np.sin(
            np.pi / 180 * 2 * argument_latitude_moon) \
                                  - 0.00083 * np.sin(np.pi / 180 * (2 * argument_latitude_moon - 2 * elongation_moon)) \
                                  + 0.00069 * np.sin(np.pi / 180 * (anomaly_moon - 2 * elongation_moon)) \
                                  + 0.0004 * np.cos(
            np.pi / 180 * (anomaly_moon + argument_latitude_moon)) - 0.00025 * np.sin(
            np.pi / 180 * 2 * anomaly_moon) \
                                  - 0.00023 * np.sin(np.pi / 180 * (anomaly_moon + 2 * argument_latitude_moon)) \
                                  + 0.0002 * np.cos(np.pi / 180 * (anomaly_moon - argument_latitude_moon)) \
                                  + 0.00019 * np.sin(np.pi / 180 * (anomaly_moon - argument_latitude_moon)) \
                                  + 0.00013 * np.sin(
            np.pi / 180 * (anomaly_moon + 2 * argument_latitude_moon - 2 * elongation_moon)) \
                                  - 0.0001 * np.cos(np.pi / 180 * (anomaly_moon - 3 * argument_latitude_moon))

        return physical_libration_node

    def get_physical_libration_longitude(self, dynamical_time_in_centuries: np.ndarray,
                                         anomaly_sun: np.ndarray) -> np.ndarray:
        """
        Get the Moon's physical libration in degrees in the longitude of the Moon.
         From ECKHARDT (1981) and MEEUS, chapter 51 (1991)
        Physical libration is the oscillation of orientation in space about uniform rotation and precession. (wikipedia)
        :param anomaly_sun: the anomaly of the sun at the dynamical time
        :param dynamical_time_in_centuries: dynamical time in centuries, computed thanks to get_dynamical_time function
        :return: the Moon's physical libration in degrees in the longitude of the Moon
        """
        # Creation of the useful terms
        anomaly_moon = self.get_mean_anomaly(dynamical_time_in_centuries)
        argument_latitude_moon = self.get_argument_latitude(dynamical_time_in_centuries)
        elongation_moon = self.get_elongation(dynamical_time_in_centuries)
        omega_moon = self.get_omega(dynamical_time_in_centuries)

        # Computation of the factor that takes into account that the Earth's eccentricity decreases
        decreasing_eccentricity_earth = 1 - 0.002516 * dynamical_time_in_centuries \
                                        - 7.4e-06 * dynamical_time_in_centuries ** 2

        # Corrective terms
        corrective_term_1 = 119.75 + 131.849 * dynamical_time_in_centuries
        corrective_term_2 = 72.56 + 20.186 * dynamical_time_in_centuries

        physical_libration_longitude = 0.0252 * decreasing_eccentricity_earth * np.sin(
            np.pi / 180 * anomaly_sun) + 0.00473 * np.sin(
            np.pi / 180 * (2 * anomaly_moon - 2 * argument_latitude_moon)) \
                                       - 0.00467 * np.sin(np.pi / 180 * anomaly_moon) + 0.00396 * np.sin(
            np.pi / 180 * corrective_term_1) \
                                       + 0.00276 * np.sin(
            np.pi / 180 * (2 * anomaly_moon - 2 * elongation_moon)) + 0.00196 * np.sin(
            np.pi / 180 * omega_moon) \
                                       - 0.00183 * np.cos(
            np.pi / 180 * (anomaly_moon - argument_latitude_moon)) + 0.00115 * np.sin(
            np.pi / 180 * (anomaly_moon - 2 * elongation_moon)) \
                                       - 0.00096 * np.sin(
            np.pi / 180 * (anomaly_moon - elongation_moon)) + 0.00046 * np.sin(
            np.pi / 180 * (2 * argument_latitude_moon - 2 * elongation_moon)) \
                                       - 0.00039 * np.sin(
            np.pi / 180 * (anomaly_moon - argument_latitude_moon)) - 0.00032 * np.sin(
            np.pi / 180 * (anomaly_moon - anomaly_sun - elongation_moon)) \
                                       + 0.00027 * np.sin(
            np.pi / 180 * (2 * anomaly_moon - anomaly_sun - 2 * elongation_moon)) + 0.00023 * np.sin(
            np.pi / 180 * corrective_term_2) \
                                       - 0.00014 * np.sin(np.pi / 180 * 2 * elongation_moon) + 0.00014 * np.cos(
            np.pi / 180 * (2 * anomaly_moon - 2 * argument_latitude_moon)) \
                                       - 0.00012 * np.sin(
            np.pi / 180 * (anomaly_moon - 2 * argument_latitude_moon)) - 0.00012 * np.sin(
            np.pi / 180 * 2 * anomaly_moon) \
                                       + 0.00011 * np.sin(
            np.pi / 180 * (2 * anomaly_moon - 2 * anomaly_sun - 2 * elongation_moon))

        return physical_libration_longitude

    def get_eccentricity(self, dynamical_time_in_centuries: np.ndarray):
        assert False, "This is not a Moon's property useful for the final computation"
