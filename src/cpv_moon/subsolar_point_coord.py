from datetime import datetime

import numpy as np

from cpv_moon.earth_moon_sun_system import EarthMoonSunSystem


class SubsolarPointCoordinate:
    """ This class computes the selenographic longitude and latitude of the subsolar point. Each instance is for a fixed
    period of time with an interval equals to one hour.
    The selenographic coordinate system is used to refer to locations on the surface of Earth's moon. Any position on
    the lunar surface can be referenced by specifying two numerical values, which are comparable to the latitude and
    longitude of Earth. (wikipedia)
    The subsolar point on a planet is the point at which its sun is perceived to be directly overhead (at the zenith);
    that is, where the sun's rays strike the planet exactly perpendicular to its surface. (wikipedia)"""

    def __init__(self, begin: datetime, end: datetime, frequency: str):
        self.earth_moon_sun_system = EarthMoonSunSystem(begin, end, frequency)

    def get_optical_seleno_longitude_sun(self) -> np.ndarray:
        """
        Get the Sun's optical longitude in degrees in the Moon's coordinate system. From MEEUS, chapter 51 (1991)
        :return: the Sun's optical longitude in degrees in the Moon's coordinate system
        """
        # Definition of the dynamical_time
        dynamical_time = self.earth_moon_sun_system.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        argument_latitude_moon = self.earth_moon_sun_system.moon.get_argument_latitude(dynamical_time)
        inclination_moon = self.earth_moon_sun_system.moon.get_inclination()
        omega_moon = self.earth_moon_sun_system.moon.get_omega(dynamical_time)
        nutation_earth = self.earth_moon_sun_system.get_nutation_earth()

        # Getting the longitude and latitude of the Moon in the Sun's coordinate system
        heliocentric_ecliptical_longitude_moon = self.earth_moon_sun_system.get_heliocentric_ecliptical_longitude_moon()

        heliocentric_ecliptical_latitude_moon = self.earth_moon_sun_system.get_heliocentric_ecliptical_lat_moon()

        # Computation of intermediate angles with geometrical considerations
        intermediate_term = heliocentric_ecliptical_longitude_moon - np.transpose(nutation_earth) - omega_moon

        geometric_factor = np.degrees(np.arctan2(np.sin(np.pi / 180 * intermediate_term)
                                                 * np.cos(np.pi / 180 * heliocentric_ecliptical_latitude_moon)
                                                 * np.cos(np.pi / 180 * inclination_moon)
                                                 - np.sin(np.pi / 180 * heliocentric_ecliptical_latitude_moon)
                                                 * np.sin(np.pi / 180 * inclination_moon),
                                                 np.cos(np.pi / 180 * intermediate_term)
                                                 * np.cos(np.pi / 180 * heliocentric_ecliptical_latitude_moon)))

        return geometric_factor - argument_latitude_moon

    def get_optical_seleno_latitude_sun(self) -> np.ndarray:
        """
        Get the Sun's optical latitude in degrees in the Moon's coordinate system. From MEEUS, chapter 51 (1991)
        :return: the Sun's optical latitude in degrees in the Moon's coordinate system
        """
        # Definition of the dynamical_time
        dynamical_time = self.earth_moon_sun_system.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        inclination_moon = self.earth_moon_sun_system.moon.get_inclination()
        omega_moon = self.earth_moon_sun_system.moon.get_omega(dynamical_time)
        nutation_earth = self.earth_moon_sun_system.get_nutation_earth()

        # Getting the longitude and latitude of the Moon in the Sun's coordinate system
        heliocentric_ecliptical_longitude_moon = self.earth_moon_sun_system.get_heliocentric_ecliptical_longitude_moon()
        heliocentric_ecliptical_latitude_moon = self.earth_moon_sun_system.get_heliocentric_ecliptical_lat_moon()

        # Computation of intermediate angle with geometrical considerations
        intermediate_angle = heliocentric_ecliptical_longitude_moon - np.transpose(nutation_earth) - omega_moon

        # Computation of the optical latitude of the Sun in the Moon's coordinate system in radians
        optical_seleno_latitude_sun_rad = np.arcsin(- np.sin(np.pi / 180 * intermediate_angle)
                                                    * np.cos(np.pi / 180 * heliocentric_ecliptical_latitude_moon)
                                                    * np.sin(np.pi / 180 * inclination_moon)
                                                    - np.sin(np.pi / 180 * heliocentric_ecliptical_latitude_moon)
                                                    * np.cos(np.pi / 180 * inclination_moon))

        return np.degrees(optical_seleno_latitude_sun_rad)

    def get_physical_seleno_longitude_sun(self) -> np.ndarray:
        """
        Get the Sun's physical longitude in degrees in the Moon's coordinate system. From MEEUS, chapter 51 (1991)
        :return: the Sun's physical longitude in degrees in the Moon's coordinate system
        """
        # Definition of the dynamical_time
        dynamical_time = self.earth_moon_sun_system.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        argument_latitude_moon = self.earth_moon_sun_system.moon.get_argument_latitude(dynamical_time)
        anomaly_sun = self.earth_moon_sun_system.sun.get_mean_anomaly(dynamical_time)

        # Definition of geometric_factor_a thanks to the function get_heliocentric_ecliptical_longitude_moon
        geometric_factor = self.get_optical_seleno_longitude_sun() + argument_latitude_moon

        # Definition of the heliocentric ecliptical latitude of the Moon
        heliocentric_ecliptical_latitude_moon = self.get_optical_seleno_latitude_sun()

        # Quantities for the calculation
        physical_libration_inclination_moon = self.earth_moon_sun_system.moon.get_physical_libration_inclination(
            dynamical_time)
        physical_libration_node_moon = self.earth_moon_sun_system.moon.get_physical_libration_node(dynamical_time)
        physical_libration_longitude_moon = self.earth_moon_sun_system.moon.get_physical_libration_longitude(
            dynamical_time, anomaly_sun)

        return - physical_libration_longitude_moon + (physical_libration_inclination_moon
                                                      * np.cos(np.pi / 180 * geometric_factor)
                                                      + physical_libration_node_moon
                                                      * np.sin(np.pi / 180 * geometric_factor)) \
               * np.tan(np.pi / 180 * heliocentric_ecliptical_latitude_moon)

    def get_physical_seleno_latitude_sun(self) -> np.ndarray:
        """
        Get the Sun's physical latitude in degrees in the Moon's coordinate system. From MEEUS, chapter 51 (1991)
        :return: the Sun's physical latitude in degrees in the Moon's coordinate system
        """
        # Definition of the dynamical_time
        dynamical_time = self.earth_moon_sun_system.get_dynamical_time()

        # Creation of the useful terms from the celestial objects
        argument_latitude_moon = self.earth_moon_sun_system.moon.get_argument_latitude(dynamical_time)

        # Definition of geometric_factor_a thanks to the function get_heliocentric_ecliptical_longitude_moon
        geometric_factor = self.get_optical_seleno_longitude_sun() + argument_latitude_moon

        # Quantities for the calculation
        physical_libration_inclination_moon = self.earth_moon_sun_system.moon.get_physical_libration_inclination(
            dynamical_time)
        physical_libration_node_moon = self.earth_moon_sun_system.moon.get_physical_libration_node(dynamical_time)

        return physical_libration_node_moon * np.cos(np.pi / 180 * geometric_factor) \
               - physical_libration_inclination_moon * np.sin(np.pi / 180 * geometric_factor)

    def get_selenographic_longitude_subsolar_point(self) -> np.ndarray:
        """
        Get the selenographic longitude of the Sun in degrees i.e the longitude of the point on the Moon's surface
        where the Sun is in zenith. From MEEUS, chapter 51 (1991)
        :return: the selenographic longitude of the Sun in degrees
        """
        # Get the terms useful for the computation
        heliocentric_ecliptical_longitude_moon = self.get_optical_seleno_longitude_sun()

        # Get the heliocentric ecliptical longitude between -180° and 180°
        heliocentric_ecliptical_longitude_moon_180 = ((heliocentric_ecliptical_longitude_moon - 180) % 360) - 180

        libration_in_longitude_moon = self.get_physical_seleno_longitude_sun()

        return heliocentric_ecliptical_longitude_moon_180 + libration_in_longitude_moon

    def get_selenographic_latitude_subsolar_point(self) -> np.ndarray:
        """
        Get the selenographic latitude of the Sun in degrees i.e the latitude of the point on the Moon's surface
        where the Sun is in zenith. From MEEUS, chapter 51 (1991)
        :return: the selenographic latitude of the Sun in degrees
        """
        # Get the terms useful for the computation
        heliocentric_ecliptical_latitude_moon = self.get_optical_seleno_latitude_sun()
        libration_in_latitude_moon = self.get_physical_seleno_latitude_sun()

        selenographic_latitude = heliocentric_ecliptical_latitude_moon + libration_in_latitude_moon

        # Get the latitude between -90° and 90°
        return ((selenographic_latitude - 90) % 180) - 90
